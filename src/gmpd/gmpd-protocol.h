/* gmpd-protocol.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_PROTOCOL_H
#define GMPD_PROTOCOL_H

#if !defined(GMPD_BUILD) && !defined(GMPD_H_INSIDE)
#   error "Only <gmpd/gmpd.h> may be included directly."
#endif

#include <gio/gio.h>

G_BEGIN_DECLS

#define GMPD_TYPE_PROTOCOL \
	(gmpd_protocol_get_type())

#define GMPD_PROTOCOL(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_PROTOCOL, GMpdProtocol))

#define GMPD_PROTOCOL_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GMPD_TYPE_PROTOCOL, GMpdProtocolClass))

#define GMPD_IS_PROTOCOL(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_PROTOCOL))

#define GMPD_IS_PROTOCOL_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GMPD_TYPE_PROTOCOL))

#define GMPD_PROTOCOL_GET_CLASS(inst) \
	(G_TYPE_INSTANCE_GET_CLASS((inst), GMPD_TYPE_PROTOCOL, GMpdProtocolClass))

typedef struct _GMpdProtocol      GMpdProtocol;
typedef struct _GMpdProtocolClass GMpdProtocolClass;

GType gmpd_protocol_get_type(void);

GMpdProtocol *gmpd_protocol_new(GSocketFamily family);

GSocketFamily gmpd_protocol_get_socket_family(GMpdProtocol *self);
gint gmpd_protocol_get_major_version(GMpdProtocol *self);
gint gmpd_protocol_get_minor_version(GMpdProtocol *self);
gint gmpd_protocol_get_patch_version(GMpdProtocol *self);

gboolean gmpd_protocol_parse_version(GMpdProtocol *self, const gchar *version_str);
gint gmpd_protocol_compare_version(GMpdProtocol *self, gint major, gint minor, gint patch);

G_END_DECLS

#endif /* GMPD_PROTOCOL_H */

