/* gmpd-single-state.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_SINGLE_STATE_H
#define GMPD_SINGLE_STATE_H

#if !defined(GMPD_BUILD) && !defined(GMPD_H_INSIDE)
#   error "Only <gmpd/gmpd.h> may be included directly."
#endif

#include <gio/gio.h>

G_BEGIN_DECLS

#define GMPD_TYPE_SINGLE_STATE \
	(gmpd_single_state_get_type())

#define GMPD_IS_SINGLE_STATE(e) \
	((e) >= GMPD_SINGLE_UNDEFINED && (e) <= GMPD_SINGLE_ENABLED)

typedef enum _GMpdSingleState {
	GMPD_SINGLE_UNDEFINED = -1,
	GMPD_SINGLE_DISABLED,
	GMPD_SINGLE_ONESHOT,
	GMPD_SINGLE_ENABLED,
} GMpdSingleState;

GType gmpd_single_state_get_type(void);

GMpdSingleState gmpd_single_state_from_string(const gchar *s);
const gchar *gmpd_single_state_to_string(GMpdSingleState state);

G_END_DECLS

#endif /* GMPD_SINGLE_STATE_H */

