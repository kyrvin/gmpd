/* gmpd.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_H
#define GMPD_H

#include <gio/gio.h>

#define GMPD_H_INSIDE

#include <gmpd/gmpd-audio-format.h>
#include <gmpd/gmpd-entity.h>
#include <gmpd/gmpd-idle.h>
#include <gmpd/gmpd-playback-state.h>
#include <gmpd/gmpd-protocol.h>
#include <gmpd/gmpd-sample-format.h>
#include <gmpd/gmpd-server-error.h>
#include <gmpd/gmpd-single-state.h>
#include <gmpd/gmpd-song.h>
#include <gmpd/gmpd-stats.h>
#include <gmpd/gmpd-status.h>
#include <gmpd/gmpd-tag.h>

#undef GMPD_H_INSIDE

#endif /* GMPD_H */

