/* gmpd-sample-format.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_SAMPLE_FORMAT_H
#define GMPD_SAMPLE_FORMAT_H

#if !defined(GMPD_BUILD) && !defined(GMPD_H_INSIDE)
#   error "Only <gmpd/gmpd.h> may be included directly."
#endif

#include <gio/gio.h>

G_BEGIN_DECLS

#define GMPD_TYPE_SAMPLE_FORMAT \
	(gmpd_sample_format_get_type())

#define GMPD_IS_SAMPLE_FORMAT(e) ( \
	(e) == GMPD_SAMPLE_UNDEFINED || \
	(e) == GMPD_SAMPLE_S8 || \
	(e) == GMPD_SAMPLE_S16 || \
	(e) == GMPD_SAMPLE_S24_P32 || \
	(e) == GMPD_SAMPLE_S32 || \
	(e) == GMPD_SAMPLE_F32 || \
	(e) == GMPD_SAMPLE_DSD \
)

typedef enum _GMpdSampleFormat {
	GMPD_SAMPLE_UNDEFINED = 0,
	GMPD_SAMPLE_S8        = 8,
	GMPD_SAMPLE_S16       = 16,
	GMPD_SAMPLE_S24_P32   = 24,
	GMPD_SAMPLE_S32       = 32,
	GMPD_SAMPLE_F32       = 0xe0,
	GMPD_SAMPLE_DSD       = 0xe1,
} GMpdSampleFormat;

GType gmpd_sample_format_get_type(void);

GMpdSampleFormat gmpd_sample_format_from_string(const gchar *s);
const gchar *gmpd_sample_format_to_string(GMpdSampleFormat format);

G_END_DECLS

#endif /* GMPD_SAMPLE_FORMAT_H */

