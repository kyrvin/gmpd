/* gmpd-playback-state.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_PLAYBACK_STATE_H
#define GMPD_PLAYBACK_STATE_H

#if !defined(GMPD_BUILD) && !defined(GMPD_H_INSIDE)
#   error "Only <gmpd/gmpd.h> may be included directly."
#endif

#include <gio/gio.h>

G_BEGIN_DECLS

#define GMPD_TYPE_PLAYBACK_STATE \
	(gmpd_playback_state_get_type())

#define GMPD_IS_PLAYBACK_STATE(e) \
	((e) >= GMPD_PLAYBACK_UNDEFINED && (e) <= GMPD_PLAYBACK_PLAYING)

typedef enum _GMpdPlaybackState {
	GMPD_PLAYBACK_UNDEFINED = -1,
	GMPD_PLAYBACK_STOPPED,
	GMPD_PLAYBACK_PAUSED,
	GMPD_PLAYBACK_PLAYING,
} GMpdPlaybackState;

GType gmpd_playback_state_get_type(void);

GMpdPlaybackState gmpd_playback_state_from_string(const gchar *s);
const gchar *gmpd_playback_state_to_string(GMpdPlaybackState state);

G_END_DECLS

#endif /* GMPD_PLAYBACK_STATE_H */

