/* gmpd-server-error.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_SERVER_ERROR_H
#define GMPD_SERVER_ERROR_H

#if !defined(GMPD_BUILD) && !defined(GMPD_H_INSIDE)
#   error "Only <gmpd/gmpd.h> may be included directly."
#endif

#include <gio/gio.h>

G_BEGIN_DECLS

#define GMPD_SERVER_ERROR \
	(gmpd_server_error_quark())

#define GMPD_IS_SERVER_ERROR(e) ( \
	((e) == GMPD_SERVER_ERROR_UNKNOWN) || \
	((e) >= GMPD_SERVER_ERROR_INVALID_LIST && (e) <= GMPD_SERVER_ERROR_INVALID_COMMAND) || \
	((e) >= GMPD_SERVER_ERROR_DOES_NOT_EXIST && (e) <= GMPD_SERVER_ERROR_EXISTS) \
)

typedef enum _GMpdServerErrorEnun {
	GMPD_SERVER_ERROR_UNKNOWN             = -1,

	GMPD_SERVER_ERROR_INVALID_LIST        = 1,
	GMPD_SERVER_ERROR_INVALID_ARGUMENTS   = 2,
	GMPD_SERVER_ERROR_INVALID_PASSWORD    = 3,
	GMPD_SERVER_ERROR_INVALID_PERMISSIONS = 4,
	GMPD_SERVER_ERROR_INVALID_COMMAND     = 5,

	GMPD_SERVER_ERROR_DOES_NOT_EXIST      = 50,
	GMPD_SERVER_ERROR_PLAYLIST_MAX        = 51,
	GMPD_SERVER_ERROR_SYSTEM              = 52,
	GMPD_SERVER_ERROR_PLAYLIST_LOAD       = 53,
	GMPD_SERVER_ERROR_UPDATE_IN_PROGRESS  = 54,
	GMPD_SERVER_ERROR_OUT_OF_SYNC         = 55,
	GMPD_SERVER_ERROR_EXISTS              = 56,
} GMpdServerErrorEnum;

GQuark gmpd_server_error_quark(void);
GError *gmpd_server_error_from_string(const gchar *ack_str);

G_END_DECLS

#endif /* GMPD_SERVER_ERROR_H */

