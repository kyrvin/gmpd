/* gmpd-string.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd-request.h"
#include "gmpd-string.h"
#include "gmpd/gmpd-protocol.h"


static void gmpd_string_request_iface_init(GMpdRequestIface *iface);

enum {
	PROP_NONE,
	PROP_INIT,
	N_PROPERTIES,
};

G_DEFINE_TYPE_WITH_CODE(GMpdString, gmpd_string, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(GMPD_TYPE_REQUEST, gmpd_string_request_iface_init))

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};

static gboolean
gmpd_string_request_format(GMpdRequest   *request,
                           GMpdProtocol  *protocol G_GNUC_UNUSED,
                           GString       *buffer,
                           GError       **error)
{
	GMpdString *self = GMPD_STRING(request);

	if (!g_utf8_validate(self->buffer->str, self->buffer->len, NULL)) {
		g_set_error(error,
		            G_IO_ERROR,
		            G_IO_ERROR_INVALID_DATA,
		            "invalid utf8 if request string");

		return FALSE;
	}

	g_string_append(buffer, self->buffer->str);
	return TRUE;
}

static void
gmpd_string_request_iface_init(GMpdRequestIface *iface)
{
	iface->format = gmpd_string_request_format;
}

static void
gmpd_string_set_property(GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
	GMpdString *self = GMPD_STRING(object);
	const gchar *init;

	switch (prop_id) {
	case PROP_INIT:
		init = g_value_get_string(value);
		if (init)
			g_string_assign(self->buffer, init);
		else
			g_string_set_size(self->buffer, 0);

		g_object_notify_by_pspec(object, PROPERTIES[PROP_INIT]);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_string_get_property(GObject    *object,
                         guint       prop_id,
                         GValue     *value   G_GNUC_UNUSED,
                         GParamSpec *pspec)
{
	G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
}

static void
gmpd_string_finalize(GObject *object)
{
	GMpdString *self = GMPD_STRING(object);

	g_string_free(self->buffer, TRUE);

	G_OBJECT_CLASS(gmpd_string_parent_class)->finalize(object);
}

static void
gmpd_string_class_init(GMpdStringClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->set_property = gmpd_string_set_property;
	object_class->get_property = gmpd_string_get_property;
	object_class->finalize = gmpd_string_finalize;

	PROPERTIES[PROP_INIT] =
		g_param_spec_string("init",
		                    "Init",
		                    "The initial value of the string",
		                    NULL,
		                    G_PARAM_WRITABLE |
		                    G_PARAM_CONSTRUCT_ONLY |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);
}

static void
gmpd_string_init(GMpdString *self)
{
	self->buffer = g_string_new(NULL);
}

GMpdString *
gmpd_string_new(const gchar *init)
{
	return g_object_new(GMPD_TYPE_STRING, "init", init, NULL);
}

const gchar *
gmpd_string_get_segment(GMpdString *self)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	return self->buffer->str;
}

gchar *
gmpd_string_dup_segment(GMpdString *self)
{
	gchar *data;

	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);

	if (!self->buffer->str)
		return NULL;

	data = g_malloc(self->buffer->len + 1);
	memcpy(data, self->buffer->str, self->buffer->len + 1);

	g_assert(data[self->buffer->len] == '\0');

	return data;
}

GMpdString *
gmpd_string_assign(GMpdString *self, const gchar *rval)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_assign(self->buffer, rval);
	return self;
}

GMpdString *
gmpd_string_vprintf(GMpdString *self, const gchar *format, va_list args)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_vprintf(self->buffer, format, args);
	return self;
}

GMpdString *
gmpd_string_append_vprintf(GMpdString *self, const gchar *format, va_list args)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_append_vprintf(self->buffer, format, args);
	return self;
}

GMpdString *
gmpd_string_printf(GMpdString *self, const gchar *format, ...)
{
	va_list args;

	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);

	va_start(args, format);
	g_string_vprintf(self->buffer, format, args);
	va_end(args);

	return self;
}

GMpdString *
gmpd_string_append_printf(GMpdString *self, const gchar *format, ...)
{
	va_list args;

	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);

	va_start(args, format);
	g_string_append_vprintf(self->buffer, format, args);
	va_end(args);

	return self;
}

GMpdString *
gmpd_string_append(GMpdString *self, const gchar *val)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_append(self->buffer, val);
	return self;
}

GMpdString *
gmpd_string_append_c(GMpdString *self, gchar c)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_append_c(self->buffer, c);
	return self;
}

GMpdString *
gmpd_string_append_unichar(GMpdString *self, gunichar wc)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_append_unichar(self->buffer, wc);
	return self;
}

GMpdString *
gmpd_string_append_len(GMpdString *self, const gchar *val, gssize len)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_append_len(self->buffer, val, len);
	return self;
}

GMpdString *
gmpd_string_append_uri_escaped(GMpdString  *self,
                               const gchar *unescaped,
                               const gchar *reserved_chars_allowed,
                               gboolean     allow_utf8)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_append_uri_escaped(self->buffer, unescaped, reserved_chars_allowed,allow_utf8);
	return self;
}

GMpdString *
gmpd_string_prepend(GMpdString *self, const gchar *val)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_prepend(self->buffer, val);
	return self;
}

GMpdString *
gmpd_string_prepend_c(GMpdString *self, gchar c)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_prepend_c(self->buffer, c);
	return self;
}

GMpdString *
gmpd_string_prepend_unichar(GMpdString *self, gunichar wc)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_prepend_unichar(self->buffer, wc);
	return self;
}

GMpdString *
gmpd_string_prepend_len(GMpdString *self, const gchar *val, gssize len)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_prepend_len(self->buffer, val, len);
	return self;
}

GMpdString *
gmpd_string_insert(GMpdString *self, gssize pos, const gchar *val)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_insert(self->buffer, pos, val);
	return self;
}

GMpdString *
gmpd_string_insert_c(GMpdString *self, gssize pos, gchar c)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_insert_c(self->buffer, pos, c);
	return self;
}

GMpdString *
gmpd_string_insert_unichar(GMpdString *self, gssize pos, gunichar wc)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_insert_unichar(self->buffer, pos, wc);
	return self;
}

GMpdString *
gmpd_string_insert_len(GMpdString *self, gssize pos, const gchar *val, gssize len)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_insert_len(self->buffer, pos, val, len);
	return self;
}

GMpdString *
gmpd_string_overwrite(GMpdString *self, gsize pos, const gchar *val)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_overwrite(self->buffer, pos, val);
	return self;
}

GMpdString *
gmpd_string_overwrite_len(GMpdString *self, gsize pos, const gchar *val, gssize len)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_overwrite_len(self->buffer, pos, val, len);
	return self;
}

GMpdString *
gmpd_string_erase(GMpdString *self, gssize pos, gssize len)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_erase(self->buffer, pos, len);
	return self;
}

GMpdString *
gmpd_string_truncate(GMpdString *self, gsize len)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_truncate(self->buffer, len);
	return self;
}

GMpdString *
gmpd_string_set_size(GMpdString *self, gsize len)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), NULL);
	g_string_set_size(self->buffer, len);
	return self;
}

guint
gmpd_string_hash(GMpdString *self)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), 0);
	return g_string_hash(self->buffer);
}

gboolean
gmpd_string_equal(GMpdString *self, GMpdString *other)
{
	g_return_val_if_fail(GMPD_IS_STRING(self), FALSE);
	g_return_val_if_fail(GMPD_IS_STRING(other), FALSE);

	return g_string_equal(self->buffer, other->buffer);
}

