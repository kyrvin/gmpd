/* gmpd-idle-object.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_IDLE_OBJECT_H
#define GMPD_IDLE_OBJECT_H

#include <gio/gio.h>
#include <gmpd/gmpd-idle.h>

G_BEGIN_DECLS

#define GMPD_TYPE_IDLE_OBJECT \
	(gmpd_idle_object_get_type())

#define GMPD_IDLE_OBJECT(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_IDLE_OBJECT, GMpdIdleObject))

#define GMPD_IDLE_OBJECT_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GMPD_TYPE_IDLE_OBJECT, GMpdIdleObjectClass))

#define GMPD_IS_IDLE_OBJECT(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_IDLE_OBJECT))

#define GMPD_IS_IDLE_OBJECT_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GMPD_TYPE_IDLE_OBJECT))

#define GMPD_IDLE_OBJECT_GET_CLASS(inst) \
	(G_TYPE_INSTANCE_GET_CLASS((inst), GMPD_TYPE_IDLE_OBJECT, GMpdIdleObjectClass))

typedef struct _GMpdIdleObject      GMpdIdleObject;
typedef struct _GMpdIdleObjectClass GMpdIdleObjectClass;

GType gmpd_idle_object_get_type(void);

GMpdIdleObject *gmpd_idle_object_new(GMpdIdle events);

void gmpd_idle_object_set_events(GMpdIdleObject *self, GMpdIdle events);
GMpdIdle gmpd_idle_object_get_events(GMpdIdleObject *self);

G_END_DECLS

#endif /* GMPD_IDLE_OBJECT_H */

