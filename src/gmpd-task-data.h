/* gmpd-task-data.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_TASK_DATA_H
#define GMPD_TASK_DATA_H

#include <gio/gio.h>
#include <gmpd-cond.h>
#include <gmpd-request.h>
#include <gmpd-response.h>

G_BEGIN_DECLS

typedef struct _GMpdTaskData {
	GMpdRequest  *request;
	GMpdResponse *response;
	GMpdCond     *cond;
} GMpdTaskData;

GMpdTaskData *gmpd_task_data_new(GMpdRequest *request, GMpdResponse *response, GMpdCond *cond);
void gmpd_task_data_free(GMpdTaskData *data);

void gmpd_task_return(GTask *task);
void gmpd_task_return_error(GTask *task, GError *err);

G_END_DECLS

#endif /* GMPD_TASK_DATA_H */

