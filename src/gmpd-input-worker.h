/* gmpd-input-worker.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_INPUT_WORKER_H
#define GMPD_INPUT_WORKER_H

#include <gio/gio.h>
#include <gmpd/gmpd-protocol.h>
#include <gmpd-worker.h>

G_BEGIN_DECLS

#define GMPD_TYPE_INPUT_WORKER \
	(gmpd_input_worker_get_type())

#define GMPD_INPUT_WORKER(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_INPUT_WORKER, GMpdInputWorker))

#define GMPD_INPUT_WORKER_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GMPD_TYPE_INPUT_WORKER, GMpdInputWorkerClass))

#define GMPD_IS_INPUT_WORKER(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_INPUT_WORKER))

#define GMPD_IS_INPUT_WORKER_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GMPD_TYPE_INPUT_WORKER))

#define GMPD_INPUT_WORKER_GET_CLASS(inst) \
	(G_TYPE_INSTANCE_GET_CLASS((inst), GMPD_TYPE_INPUT_WORKER, GMpdInputWorkerClass))

typedef struct _GMpdInputWorker      GMpdInputWorker;
typedef struct _GMpdInputWorkerClass GMpdInputWorkerClass;

GType gmpd_input_worker_get_type(void);

GMpdInputWorker *gmpd_input_worker_new(GSocket          *socket,
                                       GDataInputStream *input_stream,
                                       GMpdProtocol     *protocol);

GMpdProtocol *gmpd_input_worker_get_protocol(GMpdInputWorker *self);

G_END_DECLS

#endif /* GMPD_INPUT_WORKER_H */

