/* gmpd-string.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_STRING_H
#define GMPD_STRING_H

#include <gio/gio.h>

G_BEGIN_DECLS

#define GMPD_TYPE_STRING \
	(gmpd_string_get_type())

#define GMPD_STRING(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_STRING, GMpdString))

#define GMPD_STRING_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GMPD_TYPE_STRING, GMpdStringClass))

#define GMPD_IS_STRING(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_STRING))

#define GMPD_IS_STRING_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GMPD_TYPE_STRING))

#define GMPD_STRING_GET_CLASS(inst) \
	(G_TYPE_INSTANCE_GET_CLASS((inst), GMPD_TYPE_STRING, GMpdStringClass))

typedef struct _GMpdString      GMpdString;
typedef struct _GMpdStringClass GMpdStringClass;

struct _GMpdString {
	GObject __base__;
	GString  *buffer;
};

struct _GMpdStringClass {
	GObjectClass __base__;
};

GType gmpd_string_get_type(void);

GMpdString *gmpd_string_new(const gchar *init);

const gchar *gmpd_string_get_segment(GMpdString *self);
gchar *gmpd_string_dup_segment(GMpdString *self);

GMpdString *gmpd_string_assign(GMpdString *self, const gchar *rval);

GMpdString *gmpd_string_vprintf(GMpdString *self, const gchar *format, va_list args);
GMpdString *gmpd_string_append_vprintf(GMpdString *self, const gchar *format, va_list args);
GMpdString *gmpd_string_printf(GMpdString *self, const gchar *format, ...);
GMpdString *gmpd_string_append_printf(GMpdString *self, const gchar *format, ...);

GMpdString *gmpd_string_append(GMpdString *self, const gchar *val);
GMpdString *gmpd_string_append_c(GMpdString *self, gchar c);
GMpdString *gmpd_string_append_unichar(GMpdString *self, gunichar wc);
GMpdString *gmpd_string_append_len(GMpdString *self, const gchar *val, gssize len);
GMpdString *gmpd_string_append_uri_escaped(GMpdString *self, const gchar *unescaped, const gchar *reserved_chars_allowed, gboolean allow_utf8);

GMpdString *gmpd_string_prepend(GMpdString *self, const gchar *val);
GMpdString *gmpd_string_prepend_c(GMpdString *self, gchar c);
GMpdString *gmpd_string_prepend_unichar(GMpdString *self, gunichar wc);
GMpdString *gmpd_string_prepend_len(GMpdString *self, const gchar *val, gssize len);

GMpdString *gmpd_string_insert(GMpdString *self, gssize pos, const gchar *val);
GMpdString *gmpd_string_insert_c(GMpdString *self, gssize pos, gchar c);
GMpdString *gmpd_string_insert_unichar(GMpdString *self, gssize pos, gunichar wc);
GMpdString *gmpd_string_insert_len(GMpdString *self, gssize pos, const gchar *val, gssize len);

GMpdString *gmpd_string_overwrite(GMpdString *self, gsize pos, const gchar *val);
GMpdString *gmpd_string_overwrite_len(GMpdString *self, gsize pos, const gchar *val, gssize len);

GMpdString *gmpd_string_erase(GMpdString *self, gssize pos, gssize len);
GMpdString *gmpd_string_truncate(GMpdString *self, gsize len);
GMpdString *gmpd_string_set_size(GMpdString *self, gsize len);

guint gmpd_string_hash(GMpdString *self);
gboolean gmpd_string_equal(GMpdString *self, GMpdString *other);

G_END_DECLS

#endif /* GMPD_STRING_H */

