/* gmpd-response.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_RESPONSE_H
#define GMPD_RESPONSE_H

#include <gio/gio.h>
#include <gmpd/gmpd-protocol.h>

G_BEGIN_DECLS

#define GMPD_TYPE_RESPONSE \
	(gmpd_response_get_type())

#define GMPD_RESPONSE(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_RESPONSE, GMpdResponse))

#define GMPD_IS_RESPONSE(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_RESPONSE))

#define GMPD_RESPONSE_GET_IFACE(inst) \
	(G_TYPE_INSTANCE_GET_INTERFACE((inst), GMPD_TYPE_RESPONSE, GMpdResponseIface))

typedef struct _GMpdResponse      GMpdResponse;
typedef struct _GMpdResponseIface GMpdResponseIface;
typedef GMpdResponseIface GMpdResponseInterface;

struct _GMpdResponseIface {
	GTypeInterface __base__;

	gboolean (*feed_pair)    (GMpdResponse  *self,
	                          GMpdProtocol  *protocol,
	                          const gchar   *key,
	                          const gchar   *value,
	                          GError       **error);

	gboolean (*feed_binary)  (GMpdResponse  *self,
	                          GMpdProtocol  *protocol,
	                          GBytes        *binary,
	                          GError       **error);

	gboolean (*feed_list_ok) (GMpdResponse  *self,
	                          GMpdProtocol  *protocol,
	                          GError       **error);
};

GType gmpd_response_get_type(void);

gboolean gmpd_response_feed_pair(GMpdResponse  *self,
                                 GMpdProtocol  *protocol,
                                 const gchar   *key,
                                 const gchar   *value,
                                 GError       **error);

gboolean gmpd_response_feed_binary(GMpdResponse  *self,
                                   GMpdProtocol  *protocol,
                                   GBytes        *binary,
                                   GError       **error);

gboolean gmpd_response_feed_list_ok(GMpdResponse  *self,
                                    GMpdProtocol  *protocol,
                                    GError       **error);

gboolean gmpd_response_deserialize(GMpdResponse      *response,
                                   GMpdProtocol      *protocol,
                                   GDataInputStream  *input_stream,
                                   GError           **error);

G_END_DECLS

#endif /* GMPD_RESPONSE_H */

