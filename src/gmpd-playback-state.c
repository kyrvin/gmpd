/* gmpd-playback-state.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-playback-state.h"

static const gchar *STRINGS[] = {"stop", "pause", "play", NULL};
static const gint STRINGS_LEN = G_N_ELEMENTS(STRINGS) - 1;
static const GEnumValue VALUES[] = {
	{GMPD_PLAYBACK_UNDEFINED, "GMPD_PLAYBACK_UNDEFINED", "gmpd-playback-undefined"},
	{GMPD_PLAYBACK_STOPPED,   "GMPD_PLAYBACK_STOPPED",   "gmpd-playback-stopped"},
	{GMPD_PLAYBACK_PAUSED,    "GMPD_PLAYBACK_PAUSED",    "gmpd-playback-paused"},
	{GMPD_PLAYBACK_PLAYING,   "GMPD_PLAYBACK_PLAYING",   "gmpd-playback-playing"},
	{0, NULL, NULL},
};

GType
gmpd_playback_state_get_type(void)
{
	static GType type = 0;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		type = g_enum_register_static("GMpdPlaybackState", VALUES);
		g_once_init_leave(&init, 1);
	}

	return type;
}

GMpdPlaybackState
gmpd_playback_state_from_string(const gchar *s)
{
	GMpdPlaybackState state;

	g_return_val_if_fail(s != NULL, GMPD_PLAYBACK_UNDEFINED);

	for (state = 0; state < STRINGS_LEN; state++) {
		if (g_strcmp0(s, STRINGS[state]) == 0)
			return state;
	}

	return GMPD_PLAYBACK_UNDEFINED;
}

const gchar *
gmpd_playback_state_to_string(GMpdPlaybackState state)
{
	g_return_val_if_fail(GMPD_IS_PLAYBACK_STATE(state), NULL);

	if (state == GMPD_PLAYBACK_UNDEFINED)
		return NULL;
	else
		return STRINGS[state];
}

