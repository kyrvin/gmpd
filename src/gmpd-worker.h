/* gmpd-worker.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_WORKER_H
#define GMPD_WORKER_H

#include <gio/gio.h>
#include <gmpd-mpsc.h>

G_BEGIN_DECLS

#define GMPD_TYPE_WORKER \
	(gmpd_worker_get_type())

#define GMPD_WORKER(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_WORKER, GMpdWorker))

#define GMPD_WORKER_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GMPD_TYPE_WORKER, GMpdWorkerClass))

#define GMPD_IS_WORKER(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_WORKER))

#define GMPD_IS_WORKER_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GMPD_TYPE_WORKER))

#define GMPD_WORKER_GET_CLASS(inst) \
	(G_TYPE_INSTANCE_GET_CLASS((inst), GMPD_TYPE_WORKER, GMpdWorkerClass))

typedef struct _GMpdWorker        GMpdWorker;
typedef struct _GMpdWorkerClass   GMpdWorkerClass;
typedef struct _GMpdWorkerPrivate GMpdWorkerPrivate;

struct _GMpdWorker {
	GObject          __base__;
	GMpdWorkerPrivate *priv;
};

struct _GMpdWorkerClass {
	GObjectClass __base__;
	gboolean (*run_task) (GMpdWorker *self, GTask *task, GError **error);
};

GType gmpd_worker_get_type(void);

gboolean gmpd_worker_send_task(GMpdWorker *self, GTask *task);

G_END_DECLS

#endif /* GMPD_WORKER_H */

