/* gmpd-cond.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd-cond.h"

void
gmpd_cond_init(GMpdCond *self)
{
	g_mutex_init(&self->mutex);
	g_cond_init(&self->cond);
	self->is_ready = FALSE;

	g_mutex_lock(&self->mutex);
}

void
gmpd_cond_wait(GMpdCond *self)
{
	while (!self->is_ready)
		g_cond_wait(&self->cond, &self->mutex);
}

void
gmpd_cond_signal(GMpdCond *self)
{
	g_mutex_lock(&self->mutex);

	if (!self->is_ready) {
		self->is_ready = TRUE;
		g_cond_signal(&self->cond);
	}

	g_mutex_unlock(&self->mutex);
}

void
gmpd_cond_clear(GMpdCond *self)
{
	g_mutex_unlock(&self->mutex);

	g_cond_clear(&self->cond);
	g_mutex_clear(&self->mutex);
}

