/* gmpd-worker.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd-mpsc.h"
#include "gmpd-worker.h"

static gboolean gmpd_worker_run_task(GMpdWorker *self, GTask *task, GError **error);

struct _GMpdWorkerPrivate {
	GThread        *thread;
	GMpdMpscSender *sender;
};

typedef struct {
	GMpdWorker       *worker;
	GMpdMpscReceiver *receiver;
} ThreadFuncArgs;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE(GMpdWorker, gmpd_worker, G_TYPE_OBJECT)

static gpointer
gmpd_worker_thread_func(ThreadFuncArgs *args)
{
	GMpdWorker *worker = args->worker;
	GMpdMpscReceiver *receiver = args->receiver;
	GTask *task;
	gboolean result = TRUE;

	g_slice_free(ThreadFuncArgs, args);

	while (result && (task = gmpd_mpsc_recv(receiver))) {
		result = gmpd_worker_run_task(worker, task, NULL);
		g_object_unref(task);
	}

	gmpd_mpsc_receiver_close(receiver);

	return NULL;
}

static gboolean
gmpd_worker_run_task(GMpdWorker *self, GTask *task, GError **error)
{
	GMpdWorkerClass *klass;

	g_return_val_if_fail(GMPD_IS_WORKER(self), FALSE);
	g_return_val_if_fail(G_IS_TASK(task), FALSE);
	g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

	klass = GMPD_WORKER_GET_CLASS(self);

	g_return_val_if_fail(klass->run_task != NULL, FALSE);

	return klass->run_task(self, task, error);
}

static void
gmpd_worker_finalize(GObject *object)
{
	GMpdWorker *self = GMPD_WORKER(object);
	GMpdWorkerPrivate *priv = self->priv;

	gmpd_mpsc_sender_unref(priv->sender);
	g_thread_join(priv->thread);

	G_OBJECT_CLASS(gmpd_worker_parent_class)->finalize(object);
}

static void
gmpd_worker_class_init(GMpdWorkerClass *klass)
{
	klass->run_task = NULL;
	G_OBJECT_CLASS(klass)->finalize = gmpd_worker_finalize;
}

static void
gmpd_worker_init(GMpdWorker *self)
{
	GMpdWorkerPrivate *priv = gmpd_worker_get_instance_private(self);
	GMpdMpscChannel mpsc_channel = gmpd_mpsc_channel();
	ThreadFuncArgs *args = g_slice_new(ThreadFuncArgs);

	args->worker = self;
	args->receiver = mpsc_channel.receiver;
	priv->sender = mpsc_channel.sender;
	priv->thread = g_thread_new(NULL, (GThreadFunc)gmpd_worker_thread_func, args);

	self->priv = priv;
}

gboolean
gmpd_worker_send_task(GMpdWorker *self, GTask *task)
{
	g_return_val_if_fail(GMPD_IS_WORKER(self), FALSE);
	g_return_val_if_fail(G_IS_TASK(task), FALSE);

	return gmpd_mpsc_send(self->priv->sender, task);
}

