/* gmpd-audio-format.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-audio-format.h"
#include "gmpd/gmpd-sample-format.h"

enum {
	PROP_NONE,
	PROP_SAMPLE_RATE,
	PROP_SAMPLE_FORMAT,
	PROP_CHANNELS,
	N_PROPERTIES,
};

struct _GMpdAudioFormat {
	GObject        __base__;
	guint            sample_rate;
	GMpdSampleFormat sample_format;
	guint            channels;
};

struct _GMpdAudioFormatClass {
	GObjectClass __base__;
};

G_DEFINE_TYPE(GMpdAudioFormat, gmpd_audio_format, G_TYPE_OBJECT)

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};

static const GRegex *
gmpd_audio_format_regex(void)
{
	static const gchar pattern[] = "^(\\d+|\\*):(\\d+|\\*|f|dsd):(\\d+|\\*)$|^dsd(\\d+):(\\d+|\\*)$";
	static GRegex *regex = NULL;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		GError *err = NULL;

		regex = g_regex_new(pattern, G_REGEX_OPTIMIZE, 0, &err);
		if (!regex)
			g_error("%s: invalid regex: %s", G_STRFUNC, err->message);

		g_once_init_leave(&init, 1);
	}

	return regex;
}

static void
gmpd_audio_format_set_property(GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
	GMpdAudioFormat *self = GMPD_AUDIO_FORMAT(object);

	switch (prop_id) {
	case PROP_SAMPLE_RATE:
		gmpd_audio_format_set_sample_rate(self, g_value_get_uint(value));
		break;

	case PROP_SAMPLE_FORMAT:
		gmpd_audio_format_set_sample_format(self, g_value_get_enum(value));
		break;

	case PROP_CHANNELS:
		gmpd_audio_format_set_channels(self, g_value_get_uint(value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_audio_format_get_property(GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
	GMpdAudioFormat *self = GMPD_AUDIO_FORMAT(object);

	switch (prop_id) {
	case PROP_SAMPLE_RATE:
		g_value_set_uint(value, gmpd_audio_format_get_sample_rate(self));
		break;

	case PROP_SAMPLE_FORMAT:
		g_value_set_enum(value, gmpd_audio_format_get_sample_format(self));
		break;

	case PROP_CHANNELS:
		g_value_set_uint(value, gmpd_audio_format_get_channels(self));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_audio_format_class_init(GMpdAudioFormatClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->set_property = gmpd_audio_format_set_property;
	object_class->get_property = gmpd_audio_format_get_property;

	PROPERTIES[PROP_SAMPLE_RATE] =
		g_param_spec_uint("sample-rate",
		                  "Sample Rate",
		                  "Sample rate in bits/second",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_SAMPLE_FORMAT] =
		g_param_spec_enum("sample-format",
		                  "Sample Format",
		                  "Storage format of the samples",
		                  GMPD_TYPE_SAMPLE_FORMAT,
		                  GMPD_SAMPLE_UNDEFINED,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_CHANNELS] =
		g_param_spec_uint("channels",
		                  "Channels",
		                  "Number of audio channels",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);
}

static void
gmpd_audio_format_init(GMpdAudioFormat *self)
{
	self->sample_rate = 0;
	self->sample_format = GMPD_SAMPLE_UNDEFINED;
	self->channels = 0;
}

GMpdAudioFormat *
gmpd_audio_format_new(guint            sample_rate,
                      GMpdSampleFormat sample_format,
                      guint            channels)
{
	return g_object_new(GMPD_TYPE_AUDIO_FORMAT,
	                    "sample-rate",   sample_rate,
	                    "sample-format", sample_format,
	                    "channels",      channels,
	                    NULL);
}

GMpdAudioFormat *
gmpd_audio_format_new_from_string(const gchar *s, GError **error)
{
	const GRegex *regex = gmpd_audio_format_regex();
	GMatchInfo *match_info;
	gint match_count;
	gint pos;
	guint sample_rate;
	gint sample_format;
	guint channels;

	g_return_val_if_fail(s != NULL, NULL);
	g_return_val_if_fail(error == NULL || *error == NULL, NULL);

	if (!g_regex_match(regex, s, 0, &match_info)) {
		g_match_info_unref(match_info);

		g_set_error(error,
		            G_IO_ERROR,
		            G_IO_ERROR_INVALID_DATA,
		            "invalid mpd audio format string: %s", s);

		return NULL;
	}

	match_count = g_match_info_get_match_count(match_info);
	if (match_count == 4) {
		g_match_info_fetch_pos(match_info, 1, &pos, NULL);
		sample_rate = g_ascii_strtoull(s + pos, NULL, 10);

		g_match_info_fetch_pos(match_info, 2, &pos, NULL);
		switch (s[pos]) {
		case '*':
			sample_format = GMPD_SAMPLE_UNDEFINED;
			break;

		case 'f':
			sample_format = GMPD_SAMPLE_F32;
			break;

		case 'd':
			sample_format = GMPD_SAMPLE_DSD;
			break;

		default:
			sample_format = g_ascii_strtoull(s + pos, NULL, 10);
			if (!GMPD_IS_SAMPLE_FORMAT(sample_format)) {
				g_warn_if_fail(GMPD_IS_SAMPLE_FORMAT(sample_format));
				sample_format = GMPD_SAMPLE_UNDEFINED;
			}
		}

		g_match_info_fetch_pos(match_info, 3, &pos, NULL);
		channels = g_ascii_strtoull(s + pos, NULL, 10);


	} else if (match_count == 6) {
		g_match_info_fetch_pos(match_info, 4, &pos, NULL);
		sample_rate = g_ascii_strtoull(s + pos, NULL, 10);
		sample_rate = sample_rate * 44100 / 8;
		sample_format = GMPD_SAMPLE_DSD;

		g_match_info_fetch_pos(match_info, 5, &pos, NULL);
		channels = g_ascii_strtoull(s + pos, NULL, 10);

	} else {
		g_match_info_unref(match_info);
		g_return_val_if_reached(NULL);
	}

	g_match_info_unref(match_info);
	return gmpd_audio_format_new(sample_rate, sample_format, channels);
}

void
gmpd_audio_format_set_sample_rate(GMpdAudioFormat *self, guint sample_rate)
{
	g_return_if_fail(GMPD_IS_AUDIO_FORMAT(self));

	if (self->sample_rate != sample_rate) {
		self->sample_rate = sample_rate;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_SAMPLE_RATE]);
	}
}

void
gmpd_audio_format_set_sample_format(GMpdAudioFormat *self, GMpdSampleFormat sample_format)
{
	g_return_if_fail(GMPD_IS_AUDIO_FORMAT(self));
	g_return_if_fail(GMPD_IS_SAMPLE_FORMAT(sample_format));

	if (self->sample_format != sample_format) {
		self->sample_format = sample_format;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_SAMPLE_FORMAT]);
	}
}

void
gmpd_audio_format_set_channels(GMpdAudioFormat *self, guint channels)
{
	g_return_if_fail(GMPD_IS_AUDIO_FORMAT(self));

	if (self->channels != channels) {
		self->channels = channels;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_CHANNELS]);
	}
}

guint
gmpd_audio_format_get_sample_rate(GMpdAudioFormat *self)
{
	g_return_val_if_fail(GMPD_IS_AUDIO_FORMAT(self), 0);
	return self->sample_rate;
}

GMpdSampleFormat
gmpd_audio_format_get_sample_format(GMpdAudioFormat *self)
{
	g_return_val_if_fail(GMPD_IS_AUDIO_FORMAT(self), GMPD_SAMPLE_UNDEFINED);
	return self->sample_format;
}

guint
gmpd_audio_format_get_channels(GMpdAudioFormat *self)
{
	g_return_val_if_fail(GMPD_IS_AUDIO_FORMAT(self), 0);
	return self->channels;
}

