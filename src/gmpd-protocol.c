/* gmpd-protocol.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd.h"

static void gmpd_protocol_set_socket_family(GMpdProtocol *self, GSocketFamily socket_family);
static void gmpd_protocol_set_major_version(GMpdProtocol *self, gint major_version);
static void gmpd_protocol_set_minor_version(GMpdProtocol *self, gint minor_version);
static void gmpd_protocol_set_patch_version(GMpdProtocol *self, gint patch_version);

enum {
	PROP_NONE,
	PROP_SOCKET_FAMILY,
	PROP_MAJOR_VERSION,
	PROP_MINOR_VERSION,
	PROP_PATCH_VERSION,
	N_PROPERTIES,
};

struct _GMpdProtocol {
	GObject     __base__;
	GRWLock       rw_lock;
	GSocketFamily socket_family;
	gint          major_version;
	gint          minor_version;
	gint          patch_version;
};

struct _GMpdProtocolClass {
	GObjectClass __base__;
};

G_DEFINE_TYPE(GMpdProtocol, gmpd_protocol, G_TYPE_OBJECT)

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};

static GRegex *
gmpd_protocol_version_regex(void)
{
	static const gchar pattern[] = "OK MPD (\\d+).(\\d+).(\\d+)";
	static GRegex *regex = NULL;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		GError *err = NULL;

		regex = g_regex_new(pattern, G_REGEX_OPTIMIZE, 0, NULL);
		if (!regex)
			g_error("unable to compile regex: %s", err->message);

		g_once_init_leave(&init, 1);
	}

	return regex;
}

static void
gmpd_protocol_set_property(GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
	GMpdProtocol *self = GMPD_PROTOCOL(object);

	switch (prop_id) {
	case PROP_SOCKET_FAMILY:
		gmpd_protocol_set_socket_family(self, g_value_get_enum(value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_protocol_get_property(GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
	GMpdProtocol *self = GMPD_PROTOCOL(object);

	switch (prop_id) {
	case PROP_SOCKET_FAMILY:
		g_value_set_enum(value, gmpd_protocol_get_socket_family(self));
		break;

	case PROP_MAJOR_VERSION:
		g_value_set_int(value, gmpd_protocol_get_major_version(self));
		break;

	case PROP_MINOR_VERSION:
		g_value_set_int(value, gmpd_protocol_get_minor_version(self));
		break;

	case PROP_PATCH_VERSION:
		g_value_set_int(value, gmpd_protocol_get_patch_version(self));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_protocol_class_init(GMpdProtocolClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->set_property = gmpd_protocol_set_property;
	object_class->get_property = gmpd_protocol_get_property;

	PROPERTIES[PROP_SOCKET_FAMILY] =
		g_param_spec_enum("socket-family",
		                  "Socket Family",
		                  "The socket family of the protocol",
		                  G_TYPE_SOCKET_FAMILY,
		                  G_SOCKET_FAMILY_INVALID,
		                  G_PARAM_READWRITE |
		                  G_PARAM_CONSTRUCT_ONLY |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_MAJOR_VERSION] =
		g_param_spec_int("major-version",
		                 "Major Version",
		                 "The major version number of the protocol",
		                 0, G_MAXINT, 0,
		                 G_PARAM_READABLE |
		                 G_PARAM_EXPLICIT_NOTIFY |
		                 G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_MINOR_VERSION] =
		g_param_spec_int("minor-version",
		                 "Minor Version",
		                 "The minor version number of the protocol",
		                 0, G_MAXINT, 0,
		                 G_PARAM_READABLE |
		                 G_PARAM_EXPLICIT_NOTIFY |
		                 G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_PATCH_VERSION] =
		g_param_spec_int("patch-version",
		                 "Patch Version",
		                 "The patch version number of the protocol",
		                 0, G_MAXINT, 0,
		                 G_PARAM_READABLE |
		                 G_PARAM_EXPLICIT_NOTIFY |
		                 G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);
}

static void
gmpd_protocol_init(GMpdProtocol *self)
{
	g_rw_lock_init(&self->rw_lock);
	self->socket_family = G_SOCKET_FAMILY_INVALID;
	self->major_version = 0;
	self->minor_version = 0;
	self->patch_version = 0;
}

GMpdProtocol *
gmpd_protocol_new(GSocketFamily socket_family)
{
	return g_object_new(GMPD_TYPE_PROTOCOL, "socket-family", socket_family, NULL);
}

static void
gmpd_protocol_set_socket_family(GMpdProtocol *self, GSocketFamily socket_family)
{
	g_return_if_fail(GMPD_IS_PROTOCOL(self));
	g_return_if_fail(socket_family == G_SOCKET_FAMILY_INVALID ||
                         socket_family == G_SOCKET_FAMILY_IPV4 ||
                         socket_family == G_SOCKET_FAMILY_IPV6 ||
                         socket_family == G_SOCKET_FAMILY_UNIX);

	g_rw_lock_writer_lock(&self->rw_lock);
	g_object_freeze_notify(G_OBJECT(self));

	if (self->socket_family != socket_family) {
		self->socket_family = socket_family;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_SOCKET_FAMILY]);
	}

	g_rw_lock_writer_unlock(&self->rw_lock);
	g_object_thaw_notify(G_OBJECT(self));
}

static void
gmpd_protocol_set_major_version(GMpdProtocol *self, gint major_version)
{
	g_return_if_fail(GMPD_IS_PROTOCOL(self));
	g_return_if_fail(major_version >= 0);

	g_rw_lock_writer_lock(&self->rw_lock);
	g_object_freeze_notify(G_OBJECT(self));

	if (self->major_version != major_version) {
		self->major_version = major_version;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_MAJOR_VERSION]);
	}

	g_rw_lock_writer_unlock(&self->rw_lock);
	g_object_thaw_notify(G_OBJECT(self));
}

static void
gmpd_protocol_set_minor_version(GMpdProtocol *self, gint minor_version)
{
	g_return_if_fail(GMPD_IS_PROTOCOL(self));
	g_return_if_fail(minor_version >= 0);

	g_rw_lock_writer_lock(&self->rw_lock);
	g_object_freeze_notify(G_OBJECT(self));

	if (self->minor_version != minor_version) {
		self->minor_version = minor_version;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_MINOR_VERSION]);
	}

	g_rw_lock_writer_unlock(&self->rw_lock);
	g_object_thaw_notify(G_OBJECT(self));
}

static void
gmpd_protocol_set_patch_version(GMpdProtocol *self, gint patch_version)
{
	g_return_if_fail(GMPD_IS_PROTOCOL(self));
	g_return_if_fail(patch_version >= 0);

	g_rw_lock_writer_lock(&self->rw_lock);
	g_object_freeze_notify(G_OBJECT(self));

	if (self->patch_version != patch_version) {
		self->patch_version = patch_version;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_PATCH_VERSION]);
	}

	g_rw_lock_writer_unlock(&self->rw_lock);
	g_object_thaw_notify(G_OBJECT(self));
}

GSocketFamily
gmpd_protocol_get_socket_family(GMpdProtocol *self)
{
	GSocketFamily socket_family;

	g_return_val_if_fail(GMPD_IS_PROTOCOL(self), G_SOCKET_FAMILY_INVALID);

	g_rw_lock_reader_lock(&self->rw_lock);
	socket_family = self->socket_family;
	g_rw_lock_reader_unlock(&self->rw_lock);

	return socket_family;
}

gint
gmpd_protocol_get_major_version(GMpdProtocol *self)
{
	gint major_version;

	g_return_val_if_fail(GMPD_IS_PROTOCOL(self), 0);

	g_rw_lock_reader_lock(&self->rw_lock);
	major_version = self->major_version;
	g_rw_lock_reader_unlock(&self->rw_lock);

	return major_version;
}

gint
gmpd_protocol_get_minor_version(GMpdProtocol *self)
{
	gint minor_version;

	g_return_val_if_fail(GMPD_IS_PROTOCOL(self), 0);

	g_rw_lock_reader_lock(&self->rw_lock);
	minor_version = self->minor_version;
	g_rw_lock_reader_unlock(&self->rw_lock);

	return minor_version;
}

gint
gmpd_protocol_get_patch_version(GMpdProtocol *self)
{
	gint patch_version;

	g_return_val_if_fail(GMPD_IS_PROTOCOL(self), 0);

	g_rw_lock_reader_lock(&self->rw_lock);
	patch_version = self->patch_version;
	g_rw_lock_reader_unlock(&self->rw_lock);

	return patch_version;
}

gboolean
gmpd_protocol_parse_version(GMpdProtocol *self, const gchar *version_str)
{
	GRegex *regex = gmpd_protocol_version_regex();
	GMatchInfo *match_info;
	gint major_pos;
	gint minor_pos;
	gint patch_pos;

	g_return_val_if_fail(GMPD_IS_PROTOCOL(self), FALSE);

	if (!g_regex_match(regex, version_str, 0, &match_info)) {
		g_match_info_unref(match_info);
		return FALSE;
	}

	g_match_info_fetch_pos(match_info, 1, &major_pos, NULL);
	g_match_info_fetch_pos(match_info, 2, &minor_pos, NULL);
	g_match_info_fetch_pos(match_info, 3, &patch_pos, NULL);

	g_object_freeze_notify(G_OBJECT(self));
	gmpd_protocol_set_major_version(self, g_ascii_strtoll(version_str + major_pos, NULL, 10));
	gmpd_protocol_set_minor_version(self, g_ascii_strtoll(version_str + minor_pos, NULL, 10));
	gmpd_protocol_set_patch_version(self, g_ascii_strtoll(version_str + patch_pos, NULL, 10));
	g_object_thaw_notify(G_OBJECT(self));

	g_match_info_unref(match_info);

	return TRUE;
}

gint
gmpd_protocol_compare_version(GMpdProtocol *self,
                              gint          major_version,
                              gint          minor_version,
                              gint          patch_version)
{
	gint result;

	g_return_val_if_fail(GMPD_IS_PROTOCOL(self), -1);
	g_return_val_if_fail(major_version >= 0, -1);
	g_return_val_if_fail(minor_version >= 0, -1);
	g_return_val_if_fail(patch_version >= 0, -1);

	g_rw_lock_reader_lock(&self->rw_lock);

	(void) ((result = self->major_version - major_version) ||
	        (result = self->minor_version - minor_version) ||
	        (result = self->patch_version - patch_version));

	g_rw_lock_reader_unlock(&self->rw_lock);

	return result;
}

