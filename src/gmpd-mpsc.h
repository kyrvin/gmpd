/* gmpd-mpsc.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_MPSC_H
#define GMPD_MPSC_H

#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct _GMpdMpscSender   GMpdMpscSender;
typedef struct _GMpdMpscReceiver GMpdMpscReceiver;

typedef struct _GMpdMpscChannel {
	GMpdMpscSender   *sender;
	GMpdMpscReceiver *receiver;
} GMpdMpscChannel;

GType gmpd_mpsc_receiver_get_type(void);
GType gmpd_mpsc_sender_get_type(void);

GMpdMpscChannel gmpd_mpsc_channel(void);

void gmpd_mpsc_receiver_close(GMpdMpscReceiver *receiver);

GMpdMpscSender *gmpd_mpsc_sender_ref(GMpdMpscSender *sender);
void gmpd_mpsc_sender_unref(GMpdMpscSender *sender);

GTask *gmpd_mpsc_recv(GMpdMpscReceiver *receiver);
gboolean gmpd_mpsc_send(GMpdMpscSender *sender, GTask *task);

G_END_DECLS

#endif /* GMPD_MPSC_H */

