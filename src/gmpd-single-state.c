/* gmpd-single-state.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-single-state.h"

static const gchar *STRINGS[] = {"0", "oneshot", "1", NULL};
static const gint STRINGS_LEN = G_N_ELEMENTS(STRINGS) - 1;
static const GEnumValue VALUES[] = {
	{GMPD_SINGLE_UNDEFINED,  "GMPD_SINGLE_UNDEFINED",  "gmpd-single-undefined"},
	{GMPD_SINGLE_DISABLED,   "GMPD_SINGLE_DISABLED",   "gmpd-single-disabled"},
	{GMPD_SINGLE_ONESHOT,    "GMPD_SINGLE_ONESHOT",    "gmpd-single-oneshot"},
	{GMPD_SINGLE_ENABLED,    "GMPD_SINGLE_ENABLED",    "gmpd-single-enabled"},
	{0, NULL, NULL},
};

GType
gmpd_single_state_get_type(void)
{
	static GType type = 0;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		type = g_enum_register_static("GMpdSingleState", VALUES);
		g_once_init_leave(&init, 1);
	}

	return type;
}

GMpdSingleState
gmpd_single_state_from_string(const gchar *s)
{
	GMpdSingleState state;

	g_return_val_if_fail(s != NULL, GMPD_SINGLE_UNDEFINED);

	for (state = 0; state < STRINGS_LEN; state++) {
		if (g_strcmp0(s, STRINGS[state]) == 0)
			return state;
	}

	return GMPD_SINGLE_UNDEFINED;
}

const gchar *
gmpd_single_state_to_string(GMpdSingleState state)
{
	g_return_val_if_fail(GMPD_IS_SINGLE_STATE(state), NULL);

	if (state == GMPD_SINGLE_UNDEFINED)
		return NULL;
	else
		return STRINGS[state];
}

