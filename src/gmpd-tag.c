/* gmpd-tag.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-tag.h"

static const gchar *const STRINGS[GMPD_N_TAGS] = {
	"Artist",
	"ArtistSort",
	"Album",
	"AlbumSort",
	"AlbumArtist",
	"AlbumArtistSort",
	"Title",
	"Track",
	"Name",
	"Genre",
	"Date",
	"OriginalDate",
	"Composer",
	"CompserSort",
	"Performer",
	"Conductor",
	"Work",
	"Ensemble",
	"Movement",
	"MovementNumber",
	"Location",
	"Grouping",
	"Comment",
	"Disc",
	"Label",
	"MUSICBRAINZ_ARTISTID",
	"MUSICBRAINZ_ALBUMID",
	"MUSICBRAINZ_ALBUMARTISTID",
	"MUSICBRAINZ_TRACKID",
	"MUSICBRAINZ_RELEASETRACKID",
	"MUSICBRAINZ_WORKID",
};

static const GEnumValue VALUES[] = {
	{GMPD_TAG_UNDEFINED,                    "GMPD_TAG_UNDEFINED",                    "gmpd-tag-undefined"},
	{GMPD_TAG_ARTIST,                       "GMPD_TAG_ARTIST",                       "gmpd-tag-artist"},
	{GMPD_TAG_ARTIST_SORT,                  "GMPD_TAG_ARTIST_SORT",                  "gmpd-tag-artist-sort"},
	{GMPD_TAG_ALBUM,                        "GMPD_TAG_ALBUM",                        "gmpd-tag-album"},
	{GMPD_TAG_ALBUM_SORT,                   "GMPD_TAG_ALBUM_SORT",                   "gmpd-tag-album-sort"},
	{GMPD_TAG_ALBUM_ARTIST,                 "GMPD_TAG_ALBUM_ARTIST",                 "gmpd-tag-album-artist"},
	{GMPD_TAG_ALBUM_ARTIST_SORT,            "GMPD_TAG_ALBUM_ARTIST_SORT",            "gmpd-tag-album-artist-sort"},
	{GMPD_TAG_TITLE,                        "GMPD_TAG_TITLE",                        "gmpd-tag-title"},
	{GMPD_TAG_TRACK,                        "GMPD_TAG_TRACK",                        "gmpd-tag-track"},
	{GMPD_TAG_NAME,                         "GMPD_TAG_NAME",                         "gmpd-tag-name"},
	{GMPD_TAG_GENRE,                        "GMPD_TAG_GENRE",                        "gmpd-tag-genre"},
	{GMPD_TAG_DATE,                         "GMPD_TAG_DATE",                         "gmpd-tag-date"},
	{GMPD_TAG_ORIGINAL_DATE,                "GMPD_TAG_ORIGINAL_DATE",                "gmpd-tag-original-date"},
	{GMPD_TAG_COMPOSER,                     "GMPD_TAG_COMPOSER",                     "gmpd-tag-composer"},
	{GMPD_TAG_COMPOSER_SORT,                "GMPD_TAG_COMPOSER_SORT",                "gmpd-tag-composer-sort"},
	{GMPD_TAG_PERFORMER,                    "GMPD_TAG_PERFORMER",                    "gmpd-tag-performer"},
	{GMPD_TAG_CONDUCTOR,                    "GMPD_TAG_CONDUCTOR",                    "gmpd-tag-conductor"},
	{GMPD_TAG_WORK,                         "GMPD_TAG_WORK",                         "gmpd-tag-work"},
	{GMPD_TAG_ENSEMBLE,                     "GMPD_TAG_ENSEMBLE",                     "gmpd-tag-ensemble"},
	{GMPD_TAG_MOVEMENT,                     "GMPD_TAG_MOVEMENT",                     "gmpd-tag-movement"},
	{GMPD_TAG_MOVEMENT_NUMBER,              "GMPD_TAG_MOVEMENT_NUMBER",              "gmpd-tag-movement-number"},
	{GMPD_TAG_LOCATION,                     "GMPD_TAG_LOCATION",                     "gmpd-tag-location"},
	{GMPD_TAG_GROUPING,                     "GMPD_TAG_GROUPING",                     "gmpd-tag-grouping"},
	{GMPD_TAG_COMMENT,                      "GMPD_TAG_COMMENT",                      "gmpd-tag-comment"},
	{GMPD_TAG_DISC,                         "GMPD_TAG_DISC",                         "gmpd-tag-disc"},
	{GMPD_TAG_LABEL,                        "GMPD_TAG_LABEL",                        "gmpd-tag-label"},
	{GMPD_TAG_MUSICBRAINZ_ARTIST_ID,        "GMPD_TAG_MUSICBRAINZ_ARTIST_ID",        "gmpd-tag-musicbrainz-artist-id"},
	{GMPD_TAG_MUSICBRAINZ_ALBUM_ID,         "GMPD_TAG_MUSICBRAINZ_ALBUM_ID",         "gmpd-tag-musicbrainz-album-id"},
	{GMPD_TAG_MUSICBRAINZ_ALBUM_ARTIST_ID,  "GMPD_TAG_MUSICBRAINZ_ALBUM_ARTIST_ID",  "gmpd-tag-musicbrainz-album-artist-id"},
	{GMPD_TAG_MUSICBRAINZ_TRACK_ID,         "GMPD_TAG_MUSICBRAINZ_TRACK_ID",         "gmpd-tag-musicbrainz-track-id"},
	{GMPD_TAG_MUSICBRAINZ_RELEASE_TRACK_ID, "GMPD_TAG_MUSICBRAINZ_RELEASE_TRACK_ID", "gmpd-tag-musicbrainz-release-track-id"},
	{GMPD_TAG_MUSICBRAINZ_WORK_ID,          "GMPD_TAG_MUSICBRAINZ_WORK_ID",          "gmpd-tag-musicbrainz-work-id"},
	{0, NULL, NULL},
};

GType
gmpd_tag_get_type(void)
{
	static GType type = 0;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		type = g_enum_register_static("GMpdTag", VALUES);
		g_once_init_leave(&init, 1);
	}

	return type;
}

GMpdTag
gmpd_tag_from_string(const gchar *s)
{
	GMpdTag tag;

	g_return_val_if_fail(s != NULL, GMPD_TAG_UNDEFINED);

	for (tag = GMPD_TAG_ARTIST; tag < GMPD_N_TAGS; tag++) {
		if (g_strcmp0(s, STRINGS[tag]) == 0)
			return tag;
	}

	return GMPD_TAG_UNDEFINED;
}

const gchar *
gmpd_tag_to_string(GMpdTag tag)
{
	g_return_val_if_fail(GMPD_IS_TAG(tag), NULL);
	return tag == GMPD_TAG_UNDEFINED ? NULL : STRINGS[tag];
}

