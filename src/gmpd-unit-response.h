/* gmpd-unit-response.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_UNIT_RESPONSE_H
#define GMPD_UNIT_RESPONSE_H

#include <gio/gio.h>

G_BEGIN_DECLS

#define GMPD_TYPE_UNIT_RESPONSE \
	(gmpd_unit_response_get_type())

#define GMPD_UNIT_RESPONSE(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_UNIT_RESPONSE, GMpdUnitResponse))

#define GMPD_UNIT_RESPONSE_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GMPD_TYPE_UNIT_RESPONSE, GMpdUnitResponseClass))

#define GMPD_IS_UNIT_RESPONSE(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_UNIT_RESPONSE))

#define GMPD_IS_UNIT_RESPONSE_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GMPD_TYPE_UNIT_RESPONSE))

#define GMPD_UNIT_RESPONSE_GET_CLASS(inst) \
	(G_TYPE_INSTANCE_GET_CLASS((inst), GMPD_TYPE_UNIT_RESPONSE, GMpdUnitResponseClass))

typedef struct _GMpdUnitResponse      GMpdUnitResponse;
typedef struct _GMpdUnitResponseClass GMpdUnitResponseClass;

GType gmpd_unit_response_get_type(void);

GMpdUnitResponse *gmpd_unit_response_new(void);

G_END_DECLS

#endif /* GMPD_UNIT_RESPONSE_H */

