/* gmpd-response.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-protocol.h"
#include "gmpd/gmpd-server-error.h"
#include "gmpd-response.h"

G_DEFINE_INTERFACE(GMpdResponse, gmpd_response, G_TYPE_OBJECT)

static gboolean
gmpd_response_default_feed_pair(GMpdResponse  *self,
                                GMpdProtocol  *protocol G_GNUC_UNUSED,
                                const gchar   *key      G_GNUC_UNUSED,
                                const gchar   *value    G_GNUC_UNUSED,
                                GError       **error)
{
	GError *err = g_error_new(G_IO_ERROR,
	                          G_IO_ERROR_UNKNOWN,
	                          "%s does not implement GMpdResponse::feed_pair()",
	                          G_OBJECT_TYPE_NAME(self));

	g_critical("%s", err->message);
	g_propagate_error(error, err);

	return FALSE;
}

static gboolean
gmpd_response_default_feed_binary(GMpdResponse  *self,
                                  GMpdProtocol  *protocol G_GNUC_UNUSED,
                                  GBytes        *binary   G_GNUC_UNUSED,
                                  GError       **error)
{
	GError *err = g_error_new(G_IO_ERROR,
	                          G_IO_ERROR_UNKNOWN,
	                          "%s does not implement GMpdResponse::feed_binary()",
	                          G_OBJECT_TYPE_NAME(self));

	g_critical("%s", err->message);
	g_propagate_error(error, err);

	return FALSE;
}

static gboolean
gmpd_response_default_feed_list_ok(GMpdResponse  *self,
                                   GMpdProtocol  *protocol G_GNUC_UNUSED,
                                   GError       **error)
{
	GError *err = g_error_new(G_IO_ERROR,
	                          G_IO_ERROR_UNKNOWN,
	                          "%s does not implement GMpdResponse::feed_list_ok()",
	                          G_OBJECT_TYPE_NAME(self));

	g_critical("%s", err->message);
	g_propagate_error(error, err);

	return FALSE;
}

static void
gmpd_response_default_init(GMpdResponseIface *iface)
{
	iface->feed_pair = gmpd_response_default_feed_pair;
	iface->feed_binary = gmpd_response_default_feed_binary;
	iface->feed_list_ok = gmpd_response_default_feed_list_ok;
}

gboolean
gmpd_response_feed_pair(GMpdResponse  *self,
                        GMpdProtocol  *protocol,
                        const gchar   *key,
                        const gchar   *value,
                        GError       **error)
{
	GMpdResponseIface *iface;

	g_return_val_if_fail(GMPD_IS_RESPONSE(self), FALSE);
	g_return_val_if_fail(GMPD_IS_PROTOCOL(protocol), FALSE);
	g_return_val_if_fail(key != NULL, FALSE);
	g_return_val_if_fail(value != NULL, FALSE);
	g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

	iface = GMPD_RESPONSE_GET_IFACE(self);

	g_return_val_if_fail(iface->feed_pair != NULL, FALSE);

	return iface->feed_pair(self, protocol, key, value, error);
}

gboolean
gmpd_response_feed_binary(GMpdResponse  *self,
                          GMpdProtocol  *protocol,
                          GBytes        *binary,
                          GError       **error)
{
	GMpdResponseIface *iface;

	g_return_val_if_fail(GMPD_IS_RESPONSE(self), FALSE);
	g_return_val_if_fail(GMPD_IS_PROTOCOL(protocol), FALSE);
	g_return_val_if_fail(binary != NULL, FALSE);
	g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

	iface = GMPD_RESPONSE_GET_IFACE(self);

	g_return_val_if_fail(iface->feed_binary != NULL, FALSE);

	return iface->feed_binary(self, protocol, binary, error);
}

gboolean
gmpd_response_feed_list_ok(GMpdResponse  *self,
                           GMpdProtocol  *protocol,
                           GError       **error)
{
	GMpdResponseIface *iface;

	g_return_val_if_fail(GMPD_IS_RESPONSE(self), FALSE);
	g_return_val_if_fail(GMPD_IS_PROTOCOL(protocol), FALSE);
	g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

	iface = GMPD_RESPONSE_GET_IFACE(self);

	g_return_val_if_fail(iface->feed_list_ok != NULL, FALSE);

	return iface->feed_list_ok(self, protocol, error);
}

static GBytes *
read_binary_line(GInputStream *input_stream, gsize binary_size, GError **error)
{
	gsize buffer_len = binary_size + 1;
	gchar *buffer = g_malloc(buffer_len);
	gboolean result;
	gsize bytes_read;

	result = g_input_stream_read_all(input_stream, buffer, buffer_len, &bytes_read, NULL, error);
	if (!result) {
		g_free(buffer);
		return NULL;
	}

	if (bytes_read != buffer_len) {
		g_set_error(error,
		            G_IO_ERROR,
		            G_IO_ERROR_CLOSED,
		            "The connection was closed");

		g_free(buffer);
		return NULL;
	}

	if (buffer[binary_size] != '\n') {
		g_set_error(error,
		            G_IO_ERROR,
		            G_IO_ERROR_INVALID_DATA,
		            "Binary line is not newline terminated");

		g_free(buffer);
		return NULL;
	}

	return g_bytes_new_take(buffer, binary_size);
}

static inline gchar *
read_utf8_line(GDataInputStream *input_stream, GError **error)
{
	return g_data_input_stream_read_line_utf8(input_stream, NULL, NULL, error);
}

static gboolean
split_pair(gchar *line, gchar **key, gchar **value, GError **error)
{
	static const gchar DELIM[] = ": ";
	static const gsize DELIM_LEN = G_N_ELEMENTS(DELIM) - 1;

	gchar *delim_pos;
	*key = NULL;
	*value = NULL;

	delim_pos = g_strstr_len(line, -1, DELIM);
	if (!delim_pos) {
		g_set_error(error,
		            G_IO_ERROR,
		            G_IO_ERROR_INVALID_DATA,
		            "invalid key-value pair: %s", line);

		return FALSE;
	}

	*delim_pos = '\0';
	*key = line;
	*value = delim_pos + DELIM_LEN;

	return TRUE;
}

gboolean
gmpd_response_deserialize(GMpdResponse      *self,
                          GMpdProtocol      *protocol,
                          GDataInputStream  *input_stream,
                          GError           **error)
{
	gchar *line;
	gchar *key;
	gchar *value;
	GBytes *binary;
	gsize binary_size;

	g_return_val_if_fail(GMPD_IS_RESPONSE(self), FALSE);
	g_return_val_if_fail(GMPD_IS_PROTOCOL(protocol), FALSE);
	g_return_val_if_fail(G_IS_DATA_INPUT_STREAM(input_stream), FALSE);
	g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

	while ((line = read_utf8_line(input_stream, error))) {
		if (g_strcmp0(line, "OK") == 0) {
			g_free(line);
			return TRUE;
		}

		if (g_str_has_prefix(line, "ACK")) {
			if (error)
				*error = gmpd_server_error_from_string(line);

			g_free(line);
			return FALSE;
		}

		if (g_strcmp0(line, "list_OK") == 0) {
			gmpd_response_feed_list_ok(self, protocol, NULL);
			g_free(line);
			continue;
		}

		if (!split_pair(line, &key, &value, error)) {
			g_free(line);
			return FALSE;
		}

		if (g_strcmp0(key, "binary") == 0) {
			binary_size = g_ascii_strtoull(value, NULL, 10);
			binary = read_binary_line(G_INPUT_STREAM(input_stream), binary_size, error);
			if (!binary) {
				g_free(line);
				return FALSE;
			}

			gmpd_response_feed_binary(self, protocol, binary, NULL);
			g_bytes_unref(binary);

		} else {
			gmpd_response_feed_pair(self, protocol, key, value, NULL);
		}

		g_free(line);
	}

	return TRUE;
}
