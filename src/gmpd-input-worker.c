/* gmpd-input-worker.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-protocol.h"
#include "gmpd/gmpd-server-error.h"
#include "gmpd-idle-object.h"
#include "gmpd-input-worker.h"
#include "gmpd-response.h"
#include "gmpd-task-data.h"
#include "gmpd-worker.h"

static void gmpd_input_worker_set_socket(GMpdInputWorker *self, GSocket *socket);
static void gmpd_input_worker_set_input_stream(GMpdInputWorker *self, GDataInputStream *input_stream);
static void gmpd_input_worker_set_protocol(GMpdInputWorker *self, GMpdProtocol *protocol);

enum {
	PROP_NONE,
	PROP_SOCKET,
	PROP_INPUT_STREAM,
	PROP_PROTOCOL,
	N_PROPERTIES,
};

struct _GMpdInputWorker {
	GMpdWorker      __base__;
	GSocket          *socket;
	GDataInputStream *input_stream;
	GMpdProtocol     *protocol;
};

struct _GMpdInputWorkerClass {
	GMpdWorkerClass __base__;
};

G_DEFINE_TYPE(GMpdInputWorker, gmpd_input_worker, GMPD_TYPE_WORKER)

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};

static void
gmpd_input_worker_close(GMpdInputWorker *self)
{
	g_input_stream_close(G_INPUT_STREAM(self->input_stream), NULL, NULL);
	g_clear_object(&self->input_stream);
	g_clear_object(&self->socket);
}

static gboolean
gmpd_input_worker_run_task(GMpdWorker *worker, GTask *task, GError **error)
{
	GMpdInputWorker *self = GMPD_INPUT_WORKER(worker);
	GMpdTaskData *data = g_task_get_task_data(task);
	GError *err = NULL;
	gboolean result;

	g_return_val_if_fail(self->input_stream != NULL, FALSE);

	if (data->response == NULL) {
		g_input_stream_close(G_INPUT_STREAM(self->input_stream), NULL, NULL);
		gmpd_task_return(task);
		return FALSE;
	}

	/* The idle command does not timeout. The socket is polled here
	 * without a timeout until data is available to read, after which the
	 * timeout is respected again.
	 */
	if (GMPD_IS_IDLE_OBJECT(data->response)) {
		result = g_socket_condition_timed_wait(self->socket,
		                                       G_IO_IN | G_IO_HUP | G_IO_ERR,
		                                       -1,
		                                       g_task_get_cancellable(task),
		                                       &err);
		if (!result) {
			gmpd_input_worker_close(self);
			gmpd_task_return_error(task, g_error_copy(err));
			g_propagate_error(error, err);
			return FALSE;
		}
	}

	result = gmpd_response_deserialize(data->response, self->protocol, self->input_stream, &err);
	if (!result) {
		if (err->domain == GMPD_SERVER_ERROR) {
			gmpd_task_return_error(task, err);
			return TRUE;
		}

		gmpd_input_worker_close(self);
		gmpd_task_return_error(task, g_error_copy(err));

		g_propagate_error(error, err);
		return FALSE;
	}

	gmpd_task_return(task);
	return TRUE;
}

static void
gmpd_input_worker_set_property(GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
	GMpdInputWorker *self = GMPD_INPUT_WORKER(object);

	switch (prop_id) {
	case PROP_SOCKET:
		gmpd_input_worker_set_socket(self, g_value_get_object(value));
		break;

	case PROP_INPUT_STREAM:
		gmpd_input_worker_set_input_stream(self, g_value_get_object(value));
		break;

	case PROP_PROTOCOL:
		gmpd_input_worker_set_protocol(self, g_value_get_object(value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_input_worker_get_property(GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
	GMpdInputWorker *self = GMPD_INPUT_WORKER(object);

	switch (prop_id) {
	case PROP_PROTOCOL:
		g_value_take_object(value, gmpd_input_worker_get_protocol(self));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_input_worker_finalize(GObject *object)
{
	GMpdInputWorker *self = GMPD_INPUT_WORKER(object);

	g_clear_object(&self->socket);
	g_clear_object(&self->input_stream);
	g_clear_object(&self->protocol);

	G_OBJECT_CLASS(gmpd_input_worker_parent_class)->finalize(object);
}

static void
gmpd_input_worker_class_init(GMpdInputWorkerClass *klass)
{
	GMpdWorkerClass *worker_class = GMPD_WORKER_CLASS(klass);
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	worker_class->run_task = gmpd_input_worker_run_task;
	object_class->set_property = gmpd_input_worker_set_property;
	object_class->get_property = gmpd_input_worker_get_property;
	object_class->finalize = gmpd_input_worker_finalize;

	PROPERTIES[PROP_SOCKET] =
		g_param_spec_object("socket",
		                    "Socket",
		                    "The underlying GSocket",
		                    G_TYPE_SOCKET,
		                    G_PARAM_WRITABLE |
		                    G_PARAM_CONSTRUCT_ONLY |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_INPUT_STREAM] =
		g_param_spec_object("input-stream",
		                    "Input Stream",
		                    "The receiving end of the socket",
		                    G_TYPE_DATA_INPUT_STREAM,
		                    G_PARAM_WRITABLE |
		                    G_PARAM_CONSTRUCT_ONLY |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_PROTOCOL] =
		g_param_spec_object("protocol",
		                    "Protocol",
		                    "Protocol being spoken by the server",
		                    GMPD_TYPE_PROTOCOL,
		                    G_PARAM_READWRITE |
				    G_PARAM_CONSTRUCT_ONLY |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);
}

static void
gmpd_input_worker_init(GMpdInputWorker *self)
{
	self->socket = NULL;
	self->input_stream = NULL;
	self->protocol = NULL;
}

GMpdInputWorker *
gmpd_input_worker_new(GSocket          *socket,
                      GDataInputStream *input_stream,
                      GMpdProtocol     *protocol)
{
	return g_object_new(GMPD_TYPE_INPUT_WORKER,
	                    "socket",       socket,
	                    "input-stream", input_stream,
	                    "protocol",     protocol,
	                    NULL);
}

static void
gmpd_input_worker_set_socket(GMpdInputWorker *self, GSocket *socket)
{
	g_return_if_fail(GMPD_IS_INPUT_WORKER(self));
	g_return_if_fail(!socket || G_IS_SOCKET(socket));

	if (self->socket != socket) {
		g_clear_object(&self->socket);
		self->socket = g_object_ref(socket);
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_SOCKET]);
	}
}

static void
gmpd_input_worker_set_input_stream(GMpdInputWorker *self, GDataInputStream *input_stream)
{
	g_return_if_fail(GMPD_IS_INPUT_WORKER(self));
	g_return_if_fail(input_stream == NULL || G_IS_DATA_INPUT_STREAM(input_stream));

	if (self->input_stream != input_stream) {
		g_clear_object(&self->input_stream);
		self->input_stream = input_stream ? g_object_ref(input_stream) : NULL;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_INPUT_STREAM]);
	}
}

static void
gmpd_input_worker_set_protocol(GMpdInputWorker *self, GMpdProtocol *protocol)
{
	g_return_if_fail(GMPD_IS_INPUT_WORKER(self));
	g_return_if_fail(protocol == NULL || GMPD_IS_PROTOCOL(self));

	if (self->protocol != protocol) {
		g_clear_object(&self->protocol);
		self->protocol = protocol ? g_object_ref(protocol) : NULL;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_PROTOCOL]);
	}

}

GMpdProtocol *
gmpd_input_worker_get_protocol(GMpdInputWorker *self)
{
	g_return_val_if_fail(GMPD_IS_INPUT_WORKER(self), NULL);
	return self->protocol ? g_object_ref(self->protocol) : NULL;
}

