/* gmpd-output-worker.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_OUTPUT_WORKER_H
#define GMPD_OUTPUT_WORKER_H

#include <gio/gio.h>
#include <gmpd-input-worker.h>

G_BEGIN_DECLS

#define GMPD_TYPE_OUTPUT_WORKER \
	(gmpd_output_worker_get_type())

#define GMPD_OUTPUT_WORKER(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_OUTPUT_WORKER, GMpdOutputWorker))

#define GMPD_OUTPUT_WORKER_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GMPD_TYPE_OUTPUT_WORKER, GMpdOutputWorkerClass))

#define GMPD_IS_OUTPUT_WORKER(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_OUTPUT_WORKER))

#define GMPD_IS_OUTPUT_WORKER_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GMPD_TYPE_OUTPUT_WORKER))

#define GMPD_OUTPUT_WORKER_GET_CLASS(inst) \
	(G_TYPE_INSTANCE_GET_CLASS((inst), GMPD_TYPE_OUTPUT_WORKER, GMpdOutputWorkerClass))

typedef struct _GMpdOutputWorker      GMpdOutputWorker;
typedef struct _GMpdOutputWorkerClass GMpdOutputWorkerClass;

GType gmpd_output_worker_get_type(void);

GMpdOutputWorker *gmpd_output_worker_new(GMpdInputWorker *input_worker, GOutputStream *output_stream);

GMpdProtocol *gmpd_output_worker_get_protocol(GMpdOutputWorker *self);

G_END_DECLS

#endif /* GMPD_OUTPUT_WORKER_H */

