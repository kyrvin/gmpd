/* gmpd-output-worker.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-protocol.h"
#include "gmpd-idle-object.h"
#include "gmpd-input-worker.h"
#include "gmpd-output-worker.h"
#include "gmpd-string.h"
#include "gmpd-task-data.h"
#include "gmpd-worker.h"

static void gmpd_output_worker_set_input_worker(GMpdOutputWorker *self, GMpdInputWorker *input_worker);
static void gmpd_output_worker_set_output_stream(GMpdOutputWorker *self, GOutputStream *output_stream);
static void gmpd_output_worker_set_protocol(GMpdOutputWorker *self, GMpdProtocol *protocol);

enum {
	PROP_NONE,
	PROP_INPUT_WORKER,
	PROP_OUTPUT_STREAM,
	PROP_PROTOCOL,
	N_PROPERTIES,
};

struct _GMpdOutputWorker {
	GMpdWorker     __base__;
	GMpdInputWorker *input_worker;
	GOutputStream   *output_stream;
	GMpdProtocol    *protocol;
	GTask           *prev_idle_task;
};

struct _GMpdOutputWorkerClass {
	GMpdWorkerClass __base__;
};

G_DEFINE_TYPE(GMpdOutputWorker, gmpd_output_worker, GMPD_TYPE_WORKER)

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};

static gboolean
gmpd_output_worker_run_task(GMpdWorker *worker, GTask *task, GError **error)
{
	GMpdOutputWorker *self = GMPD_OUTPUT_WORKER(worker);
	GMpdTaskData *data = g_task_get_task_data(task);
	gboolean result;
	GError *err = NULL;

	if (self->prev_idle_task) {
		/* The previous task serialized over the stream was an idle
		 * request. The request needs to be cancelled at the protocol
		 * level or MPD will disconnect us. This is accomplished by
		 * sending the noidle request.
		 *
		 * The check immediately below is slightly overzealous and will
		 * occasionally send the noidle request even if the previous
		 * idle request has returned or is returning at the protocol
		 * level. While not the ideal behavior, this results in a
		 * protocol no-op.
		 */
		if (!g_task_get_completed(self->prev_idle_task)) {
			GMpdRequest *noidle = GMPD_REQUEST(gmpd_string_new("noidle\n"));

			result = gmpd_request_serialize(noidle, self->protocol, self->output_stream, &err);
			g_object_unref(noidle);

			if (!result) {
				gmpd_task_return_error(task, g_error_copy(err));
				g_propagate_error(error, err);

				g_clear_object(&self->prev_idle_task);
				g_clear_object(&self->input_worker);
				g_output_stream_close(self->output_stream, NULL, NULL);

				return FALSE;
			}
		}

		g_clear_object(&self->prev_idle_task);
	}

	if (!gmpd_request_serialize(data->request, self->protocol, self->output_stream, &err) ||
	    !g_output_stream_flush(self->output_stream, NULL, &err)) {
		gmpd_task_return_error(task, g_error_copy(err));
		g_propagate_error(error, err);

		g_clear_object(&self->input_worker);
		g_output_stream_close(self->output_stream, NULL, NULL);

		return FALSE;
	}

	if (GMPD_IS_IDLE_OBJECT(data->request))
		self->prev_idle_task = g_object_ref(task);

	if (!gmpd_worker_send_task(GMPD_WORKER(self->input_worker), g_object_ref(task))) {
		GError *err = g_error_new_literal(G_IO_ERROR,
		                                  G_IO_ERROR_CANCELLED,
		                                  "The operation was cancelled");

		gmpd_task_return_error(task, err);
		g_object_unref(task);

		return FALSE;
	}

	return TRUE;
}

static void
gmpd_output_worker_set_property(GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
	GMpdOutputWorker *self = GMPD_OUTPUT_WORKER(object);

	switch (prop_id) {
	case PROP_INPUT_WORKER:
		gmpd_output_worker_set_input_worker(self, g_value_get_object(value));
		break;

	case PROP_OUTPUT_STREAM:
		gmpd_output_worker_set_output_stream(self, g_value_get_object(value));
		break;

	case PROP_PROTOCOL:
		gmpd_output_worker_set_protocol(self, g_value_get_object(value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_output_worker_get_property(GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
	GMpdOutputWorker *self = GMPD_OUTPUT_WORKER(object);

	switch (prop_id) {
	case PROP_PROTOCOL:
		g_value_take_object(value, gmpd_output_worker_get_protocol(self));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_output_worker_finalize(GObject *object)
{
	GMpdOutputWorker *self = GMPD_OUTPUT_WORKER(object);

	g_clear_object(&self->input_worker);
	g_clear_object(&self->output_stream);
	g_clear_object(&self->protocol);
	g_clear_object(&self->prev_idle_task);

	G_OBJECT_CLASS(gmpd_output_worker_parent_class)->finalize(object);
}

static void
gmpd_output_worker_class_init(GMpdOutputWorkerClass *klass)
{
	GMpdWorkerClass *worker_class = GMPD_WORKER_CLASS(klass);
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	worker_class->run_task = gmpd_output_worker_run_task;
	object_class->set_property = gmpd_output_worker_set_property;
	object_class->get_property = gmpd_output_worker_get_property;
	object_class->finalize = gmpd_output_worker_finalize;

	PROPERTIES[PROP_INPUT_WORKER] =
		g_param_spec_object("input-worker",
		                    "Input Worker",
		                    "The input worker to chain up to",
		                    GMPD_TYPE_INPUT_WORKER,
		                    G_PARAM_WRITABLE |
		                    G_PARAM_CONSTRUCT_ONLY |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_OUTPUT_STREAM] =
		g_param_spec_object("output-stream",
		                    "Output Stream",
		                    "The output stream to operate on",
		                    G_TYPE_OUTPUT_STREAM,
		                    G_PARAM_WRITABLE |
		                    G_PARAM_CONSTRUCT_ONLY |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_PROTOCOL] =
		g_param_spec_object("protocol",
		                    "Protocol",
		                    "Protocol version information",
		                    GMPD_TYPE_PROTOCOL,
		                    G_PARAM_READABLE |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);
}

static void
gmpd_output_worker_init(GMpdOutputWorker *self)
{
	self->input_worker = NULL;
	self->output_stream = NULL;
	self->protocol = NULL;
	self->prev_idle_task = NULL;
}

GMpdOutputWorker *
gmpd_output_worker_new(GMpdInputWorker *input_worker, GOutputStream *output_stream)
{
	GMpdProtocol *protocol;
	GMpdOutputWorker *output_worker;

	g_return_val_if_fail(GMPD_IS_INPUT_WORKER(input_worker), NULL);
	g_return_val_if_fail(G_IS_OUTPUT_STREAM(output_stream), NULL);

	protocol = gmpd_input_worker_get_protocol(input_worker);

	output_worker = g_object_new(GMPD_TYPE_OUTPUT_WORKER,
	                             "input-worker",  input_worker,
	                             "output-stream", output_stream,
	                             "protocol",      protocol,
	                             NULL);

	g_object_unref(protocol);
	return output_worker;
}

static void
gmpd_output_worker_set_input_worker(GMpdOutputWorker *self, GMpdInputWorker *input_worker)
{
	g_return_if_fail(GMPD_IS_OUTPUT_WORKER(self));
	g_return_if_fail(input_worker == NULL || GMPD_IS_INPUT_WORKER(input_worker));

	if (self->input_worker != input_worker) {
		g_clear_object(&self->input_worker);
		self->input_worker = input_worker ? g_object_ref(input_worker) : NULL;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_INPUT_WORKER]);
	}
}

static void
gmpd_output_worker_set_output_stream(GMpdOutputWorker *self, GOutputStream *output_stream)
{
	g_return_if_fail(GMPD_IS_OUTPUT_WORKER(self));
	g_return_if_fail(output_stream == NULL || G_IS_OUTPUT_STREAM(output_stream));

	if (self->output_stream != output_stream) {
		g_clear_object(&self->output_stream);
		self->output_stream = output_stream ? g_object_ref(output_stream) : NULL;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_OUTPUT_STREAM]);
	}
}

static void
gmpd_output_worker_set_protocol(GMpdOutputWorker *self, GMpdProtocol *protocol)
{
	g_return_if_fail(GMPD_IS_OUTPUT_WORKER(self));
	g_return_if_fail(protocol == NULL || G_IS_OUTPUT_STREAM(protocol));

	if (self->protocol != protocol) {
		g_clear_object(&self->protocol);
		self->protocol = protocol ? g_object_ref(protocol) : NULL;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_PROTOCOL]);
	}
}

GMpdProtocol *
gmpd_output_worker_get_protocol(GMpdOutputWorker *self)
{
	g_return_val_if_fail(GMPD_IS_OUTPUT_WORKER(self), NULL);
	return self->protocol ? g_object_ref(self->protocol) : NULL;
}

