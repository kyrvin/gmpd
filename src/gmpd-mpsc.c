/* gmpd-mpsc.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd-mpsc.h"

static GMpdMpscReceiver *gmpd_mpsc_receiver_new(void);
static GMpdMpscReceiver *gmpd_mpsc_receiver_ref(GMpdMpscReceiver *receiver);
static void gmpd_mpsc_receiver_unref(GMpdMpscReceiver *receiver);
static GMpdMpscSender *gmpd_mpsc_sender_new(GMpdMpscReceiver *receiver);

struct _GMpdMpscSender {
	gint              ref_count;
	GMpdMpscReceiver *receiver;
};

struct _GMpdMpscReceiver {
	gint    ref_count;
	GMutex  mutex;
	GCond   cond;
	GQueue *queue;
};

GMpdMpscChannel
gmpd_mpsc_channel(void)
{
	GMpdMpscChannel channel;

	channel.receiver = gmpd_mpsc_receiver_new();
	channel.sender = gmpd_mpsc_sender_new(channel.receiver);

	return channel;
}

static GMpdMpscReceiver *
gmpd_mpsc_receiver_new(void)
{
	GMpdMpscReceiver *receiver = g_slice_new(GMpdMpscReceiver);

	receiver->ref_count = 1;
	g_mutex_init(&receiver->mutex);
	g_cond_init(&receiver->cond);
	receiver->queue = g_queue_new();

	return receiver;
}

static GMpdMpscReceiver *
gmpd_mpsc_receiver_ref(GMpdMpscReceiver *receiver)
{
	g_return_val_if_fail(receiver != NULL, NULL);
	g_return_val_if_fail(receiver->ref_count >= 1, NULL);

	g_atomic_int_inc(&receiver->ref_count);
	return receiver;
}

static void
gmpd_mpsc_receiver_unref(GMpdMpscReceiver *receiver)
{
	g_return_if_fail(receiver != NULL);
	g_return_if_fail(receiver->ref_count >= 1);

	if (g_atomic_int_dec_and_test(&receiver->ref_count)) {
		if (receiver->queue)
			g_queue_free_full(receiver->queue, g_object_unref);

		g_cond_clear(&receiver->cond);
		g_mutex_clear(&receiver->mutex);

		g_slice_free(GMpdMpscReceiver, receiver);
	}
}

void
gmpd_mpsc_receiver_close(GMpdMpscReceiver *receiver)
{
	GTask *task;
	GError *err;

	g_return_if_fail(receiver != NULL);

	g_mutex_lock(&receiver->mutex);

	if (receiver->queue) {
		while ((task = g_queue_pop_head(receiver->queue))) {
			err = g_error_new_literal(G_IO_ERROR,
			                          G_IO_ERROR_CANCELLED,
			                          "The operation was cancelled");

			g_task_return_error(task, err);
			g_object_unref(task);
		}

		g_queue_free_full(receiver->queue, g_object_unref);
		receiver->queue = NULL;

		g_cond_signal(&receiver->cond);
	}

	g_mutex_unlock(&receiver->mutex);
	gmpd_mpsc_receiver_unref(receiver);
}

static GMpdMpscSender *
gmpd_mpsc_sender_new(GMpdMpscReceiver *receiver)
{
	GMpdMpscSender *sender;

	g_return_val_if_fail(receiver != NULL, NULL);

	sender = g_slice_new(GMpdMpscSender);

	sender->ref_count = 1;
	sender->receiver = gmpd_mpsc_receiver_ref(receiver);

	return sender;
}

GMpdMpscSender *
gmpd_mpsc_sender_ref(GMpdMpscSender *sender)
{
	g_return_val_if_fail(sender != NULL, NULL);
	g_return_val_if_fail(sender->ref_count >= 1, NULL);

	g_atomic_int_inc(&sender->ref_count);
	return sender;
}

void
gmpd_mpsc_sender_unref(GMpdMpscSender *sender)
{
	g_return_if_fail(sender != NULL);
	g_return_if_fail(sender->ref_count >= 1);

	if (g_atomic_int_dec_and_test(&sender->ref_count)) {
		gmpd_mpsc_receiver_close(sender->receiver);
		g_slice_free(GMpdMpscSender, sender);
	}
}

GTask *
gmpd_mpsc_recv(GMpdMpscReceiver *receiver)
{
	GTask *task;

	g_return_val_if_fail(receiver != NULL, NULL);

	g_mutex_lock(&receiver->mutex);

	while (receiver->queue && receiver->queue->length == 0)
		g_cond_wait(&receiver->cond, &receiver->mutex);

	task = receiver->queue ? G_TASK(g_queue_pop_head(receiver->queue)) : NULL;
	g_mutex_unlock(&receiver->mutex);

	return task;
}

gboolean
gmpd_mpsc_send(GMpdMpscSender *sender, GTask *task)
{
	GMpdMpscReceiver *receiver;

	g_return_val_if_fail(sender != NULL, FALSE);
	g_return_val_if_fail(G_IS_TASK(task), FALSE);

	receiver = sender->receiver;

	g_mutex_lock(&receiver->mutex);

	if (!receiver->queue) {
		g_mutex_unlock(&receiver->mutex);
		return FALSE;
	}

	g_queue_push_tail(receiver->queue, task);

	g_cond_signal(&receiver->cond);
	g_mutex_unlock(&receiver->mutex);

	return TRUE;
}
