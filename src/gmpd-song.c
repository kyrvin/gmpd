/* gmpd-song.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-audio-format.h"
#include "gmpd/gmpd-entity.h"
#include "gmpd/gmpd-protocol.h"
#include "gmpd/gmpd-song.h"
#include "gmpd/gmpd-tag.h"
#include "gmpd-entity-priv.h"
#include "gmpd-response.h"

static void gmpd_song_response_iface_init(GMpdResponseIface *iface);

enum {
	PROP_NONE,
	PROP_POSITION,
	PROP_ID,
	PROP_PRIORITY,
	PROP_DURATION,
	PROP_RANGE_START,
	PROP_RANGE_END,
	PROP_FORMAT,
	N_PROPERTIES,
};

enum {
	SIGNAL_TAG_CHANGED,
	N_SIGNALS,
};

struct _GMpdSong {
	GMpdEntity __base__;
	guint position;
	guint id;
	guint priority;
	gfloat duration;
	gfloat range_start;
	gfloat range_end;
	GMpdAudioFormat *format;
	GHashTable *tags[GMPD_N_TAGS];
};

struct _GMpdSongClass {
	GMpdEntityClass __base__;
	void (*tag_changed) (GMpdSong *self, GMpdTag tag);
};

G_DEFINE_TYPE_WITH_CODE(GMpdSong, gmpd_song, GMPD_TYPE_ENTITY,
                        G_IMPLEMENT_INTERFACE(GMPD_TYPE_RESPONSE, gmpd_song_response_iface_init))

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};
static guint SIGNALS[N_SIGNALS] = {0};

static gboolean
gmpd_song_response_feed_pair(GMpdResponse  *response,
                             GMpdProtocol  *protocol G_GNUC_UNUSED,
                             const gchar   *key,
                             const gchar   *value,
                             GError       **error)
{
	GMpdSong *self = GMPD_SONG(response);
	GMpdTag tag;

	if (g_strcmp0(key, "file") == 0) {
		gmpd_entity_set_path(GMPD_ENTITY(self), value);

	} else if (g_strcmp0(key, "Last-Modified") == 0) {
		GDateTime *last_modified = g_date_time_new_from_iso8601(value, NULL);
		gmpd_entity_set_last_modified(GMPD_ENTITY(self), last_modified);
		g_clear_pointer(&last_modified, g_date_time_unref);

	} else if (g_strcmp0(key, "Pos") == 0) {
		guint position = g_ascii_strtoull(value, NULL, 10);
		gmpd_song_set_position(self, position);

	} else if (g_strcmp0(key, "Id") == 0) {
		guint id = g_ascii_strtoull(value, NULL, 10);
		gmpd_song_set_id(self, id);

	} else if (g_strcmp0(key, "Prio") == 0) {
		guint priority = g_ascii_strtoull(value, NULL, 10);
		if (priority > G_MAXUINT8)
			priority = G_MAXUINT8;

		gmpd_song_set_priority(self, priority);

	} else if (g_strcmp0(key, "Time") == 0 && !self->duration) {
		gfloat duration = g_ascii_strtoull(value, NULL, 10);
		if (duration < 0)
			duration = 0;

		gmpd_song_set_duration(self, duration);

	} else if (g_strcmp0(key, "duration") == 0) {
		gfloat duration = g_ascii_strtod(value, NULL);
		if (duration < 0)
			duration = 0;

		gmpd_song_set_duration(self, duration);

	} else if (g_strcmp0(key, "Range") == 0) {
		const gchar *end = NULL;
		gfloat range_start;
		gfloat range_end;

		range_start = g_ascii_strtod(value, (gchar **)&end);
		if (!end || *end != '-') {
			GError *err = g_error_new(G_IO_ERROR,
			                          G_IO_ERROR_INVALID_DATA,
			                          "%s: invalid value for key %s: %s",
			                          G_OBJECT_TYPE_NAME(self), key, value);

			g_warning("%s", err->message);
			g_propagate_error(error, err);

			return FALSE;
		}

		range_end = g_ascii_strtod(end + 1, NULL);

		gmpd_song_set_range_start(self, range_start);
		gmpd_song_set_range_end(self, range_end);

	} else if (g_strcmp0(key, "Format") == 0) {
		GMpdAudioFormat *format = gmpd_audio_format_new_from_string(value, NULL);
		gmpd_song_set_format(self, format);
		g_clear_object(&format);

	} else if ((tag = gmpd_tag_from_string(key)) != GMPD_TAG_UNDEFINED) {
		gmpd_song_add_tag(self, tag, value);

	} else {
		GError *err = g_error_new(G_IO_ERROR,
		                          G_IO_ERROR_INVALID_DATA,
		                          "invalid key for %s: %s",
		                          G_OBJECT_TYPE_NAME(self), key);

		g_warning("%s", err->message);
		g_propagate_error(error, err);

		return FALSE;
	}

	return TRUE;
}

static void
gmpd_song_response_iface_init(GMpdResponseIface *iface)
{
	iface->feed_pair = gmpd_song_response_feed_pair;
}

static void
gmpd_song_tag_changed(GMpdSong *self, GMpdTag tag)
{
	static const gchar *const QUARK_STRINGS[GMPD_N_TAGS] = {
		"artist",
		"artist-sort",
		"album",
		"album-sort",
		"album-artist",
		"album-artist-sort",
		"title",
		"track",
		"name",
		"genre",
		"date",
		"original-date",
		"composer",
		"composer-sort",
		"performer",
		"conductor",
		"work",
		"ensemble",
		"movement",
		"movement-number",
		"location",
		"grouping",
		"comment",
		"disc",
		"label",
		"musicbrainz-artist-id",
		"musicbrainz-album-id",
		"musicbrainz-album-artist-id",
		"musicbrainz-track-id",
		"musicbrainz-release-track-id",
		"musicbrainz-work-id",
	};
	static GQuark QUARKS[GMPD_N_TAGS] = {0};
	static gsize init = 0;

	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(GMPD_IS_TAG(tag) && tag != GMPD_TAG_UNDEFINED);

	if (g_once_init_enter(&init)) {
		gint i;

		for (i = GMPD_TAG_ARTIST; i < GMPD_N_TAGS; i++)
			QUARKS[i] = g_quark_from_static_string(QUARK_STRINGS[i]);

		g_once_init_leave(&init, 1);
	}

	g_signal_emit(self, SIGNALS[SIGNAL_TAG_CHANGED], QUARKS[tag], tag);
}

static void
gmpd_song_set_property(GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
	GMpdSong *self = GMPD_SONG(object);

	switch (prop_id) {
	case PROP_POSITION:
		gmpd_song_set_position(self, g_value_get_uint(value));
		break;

	case PROP_ID:
		gmpd_song_set_id(self, g_value_get_uint(value));
		break;

	case PROP_PRIORITY:
		gmpd_song_set_priority(self, g_value_get_uint(value));
		break;

	case PROP_DURATION:
		gmpd_song_set_duration(self, g_value_get_float(value));
		break;

	case PROP_RANGE_START:
		gmpd_song_set_range_start(self, g_value_get_float(value));
		break;

	case PROP_RANGE_END:
		gmpd_song_set_range_end(self, g_value_get_float(value));
		break;

	case PROP_FORMAT:
		gmpd_song_set_format(self, g_value_get_object(value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_song_get_property(GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
	GMpdSong *self = GMPD_SONG(object);

	switch (prop_id) {
	case PROP_POSITION:
		g_value_set_uint(value, gmpd_song_get_position(self));
		break;

	case PROP_ID:
		g_value_set_uint(value, gmpd_song_get_id(self));
		break;

	case PROP_PRIORITY:
		g_value_set_uint(value, gmpd_song_get_priority(self));
		break;

	case PROP_DURATION:
		g_value_set_float(value, gmpd_song_get_duration(self));
		break;

	case PROP_RANGE_START:
		g_value_set_float(value, gmpd_song_get_range_start(self));
		break;

	case PROP_RANGE_END:
		g_value_set_float(value, gmpd_song_get_range_end(self));
		break;

	case PROP_FORMAT:
		g_value_take_object(value, gmpd_song_get_format(self));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_song_finalize(GObject *object)
{
	GMpdSong *self = GMPD_SONG(object);
	GMpdTag i;

	g_clear_object(&self->format);

	for (i = GMPD_TAG_ARTIST; i < GMPD_N_TAGS; i++)
		g_clear_pointer(&self->tags[i], g_hash_table_unref);

	G_OBJECT_CLASS(gmpd_song_parent_class)->finalize(object);
}

static void
gmpd_song_class_init(GMpdSongClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->set_property = gmpd_song_set_property;
	object_class->get_property = gmpd_song_get_property;
	object_class->finalize = gmpd_song_finalize;

	PROPERTIES[PROP_POSITION] =
		g_param_spec_uint("position",
		                  "Position",
		                  "Queue position of the song.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_ID] =
		g_param_spec_uint("id",
		                  "Id",
		                  "Queue Id of the song.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);
	PROPERTIES[PROP_PRIORITY] =
		g_param_spec_uint("priority",
		                  "Priority",
		                  "Queue priority of the song.",
		                  0, G_MAXUINT8, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_DURATION] =
		g_param_spec_float("duration",
		                   "Duration",
		                   "Duration of the song.",
		                   0, G_MAXFLOAT, 0,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_RANGE_START] =
		g_param_spec_float("range-start",
		                   "Range Start",
		                   "Start position of the playback range.",
		                   0, G_MAXFLOAT, 0,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_RANGE_END] =
		g_param_spec_float("range-end",
		                   "Range End",
		                   "End position of the playback range.",
		                   0, G_MAXFLOAT, 0,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_FORMAT] =
		g_param_spec_object("format",
		                    "Format",
		                    "Audio format of the song.",
		                    GMPD_TYPE_AUDIO_FORMAT,
		                    G_PARAM_READWRITE |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);

	SIGNALS[SIGNAL_TAG_CHANGED] =
		g_signal_new("tag-changed",
		             GMPD_TYPE_SONG,
		             G_SIGNAL_RUN_FIRST | G_SIGNAL_DETAILED,
		             G_STRUCT_OFFSET(GMpdSongClass, tag_changed),
		             NULL, NULL, NULL,
		             G_TYPE_NONE,
		             1, GMPD_TYPE_TAG);
}

static void
gmpd_song_init(GMpdSong *self)
{
	GMpdTag i;

	self->position = 0;
	self->id = 0;
	self->priority = 0;
	self->duration = 0;
	self->range_start = 0;
	self->range_end = 0;
	self->format = NULL;

	for (i = GMPD_TAG_ARTIST; i < GMPD_N_TAGS; i++)
		self->tags[i] = NULL;
}

GMpdSong *
gmpd_song_new(void)
{
	return g_object_new(GMPD_TYPE_SONG, NULL);
}

void
gmpd_song_set_position(GMpdSong *self, guint position)
{
	g_return_if_fail(GMPD_IS_SONG(self));

	if (self->position != position) {
		self->position = position;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_POSITION]);
	}
}

void
gmpd_song_set_id(GMpdSong *self, guint id)
{
	g_return_if_fail(GMPD_IS_SONG(self));

	if (self->id != id) {
		self->id = id;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_ID]);
	}
}

void
gmpd_song_set_priority(GMpdSong *self, guint priority)
{
	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(priority <= G_MAXUINT8);

	if (self->priority != priority) {
		self->priority = priority;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_PRIORITY]);
	}
}

void
gmpd_song_set_duration(GMpdSong *self, gfloat duration)
{
	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(duration >= 0);

	if (self->duration != duration) {
		self->duration = duration;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_DURATION]);
	}
}

void
gmpd_song_set_range_start(GMpdSong *self, gfloat range_start)
{
	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(range_start >= 0);

	if (self->range_start != range_start) {
		self->range_start = range_start;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_RANGE_START]);
	}
}

void
gmpd_song_set_range_end(GMpdSong *self, gfloat range_end)
{
	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(range_end >= 0);

	if (self->range_end != range_end) {
		self->range_end = range_end;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_RANGE_END]);
	}
}

void
gmpd_song_set_format(GMpdSong *self, GMpdAudioFormat *format)
{
	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(format == NULL || GMPD_IS_AUDIO_FORMAT(format));

	if (self->format != format) {
		g_clear_object(&self->format);
		self->format = format ? g_object_ref(format) : NULL;

		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_FORMAT]);
	}
}

guint
gmpd_song_get_position(GMpdSong *self)
{
	g_return_val_if_fail(GMPD_IS_SONG(self), 0);
	return self->position;
}

guint
gmpd_song_get_id(GMpdSong *self)
{
	g_return_val_if_fail(GMPD_IS_SONG(self), 0);
	return self->id;
}

guint
gmpd_song_get_priority(GMpdSong *self)
{
	g_return_val_if_fail(GMPD_IS_SONG(self), 0);
	return self->priority;
}

gfloat
gmpd_song_get_duration(GMpdSong *self)
{
	g_return_val_if_fail(GMPD_IS_SONG(self), 0);
	return self->duration;
}

gfloat
gmpd_song_get_range_start(GMpdSong *self)
{
	g_return_val_if_fail(GMPD_IS_SONG(self), 0);
	return self->range_start;
}

gfloat
gmpd_song_get_range_end(GMpdSong *self)
{
	g_return_val_if_fail(GMPD_IS_SONG(self), 0);
	return self->range_end;
}

GMpdAudioFormat *
gmpd_song_get_format(GMpdSong *self)
{
	g_return_val_if_fail(GMPD_IS_SONG(self), NULL);
	return self->format ? g_object_ref(self->format) : NULL;
}

static inline GHashTable *
gmpd_song_get_tag_table(GMpdSong *self, GMpdTag tag)
{
	if (!self->tags[tag])
		self->tags[tag] = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);

	return self->tags[tag];
}

void
gmpd_song_add_tag(GMpdSong *self, GMpdTag tag, const gchar *value)
{
	GHashTable *tag_table;

	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(GMPD_IS_TAG(tag) && tag != GMPD_TAG_UNDEFINED);
	g_return_if_fail(value != NULL);

	tag_table = gmpd_song_get_tag_table(self, tag);

	if (g_hash_table_insert(tag_table, g_strdup(value), NULL))
		gmpd_song_tag_changed(self, tag);
}

void
gmpd_song_remove_tag(GMpdSong *self, GMpdTag tag, const gchar *value)
{
	GHashTable *tag_table;

	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(GMPD_IS_TAG(tag) && tag != GMPD_TAG_UNDEFINED);
	g_return_if_fail(value != NULL);

	tag_table = gmpd_song_get_tag_table(self, tag);

	if (g_hash_table_remove(tag_table, value))
		gmpd_song_tag_changed(self, tag);
}

static void
foreach_tag_callback(gchar *key, gchar *value, GPtrArray *array)
{
	g_return_if_fail(key != NULL);
	g_return_if_fail(value == NULL);

	g_ptr_array_add(array, g_strdup(key));
}

gchar **
gmpd_song_get_tag(GMpdSong *self, GMpdTag tag)
{
	GHashTable *tag_table;
	GPtrArray *array;

	g_return_val_if_fail(GMPD_IS_SONG(self), NULL);
	g_return_val_if_fail(GMPD_IS_TAG(tag) && tag != GMPD_TAG_UNDEFINED, NULL);

	tag_table = gmpd_song_get_tag_table(self, tag);
	array = g_ptr_array_new();

	g_hash_table_foreach(tag_table, (GHFunc)foreach_tag_callback, array);
	g_ptr_array_add(array, NULL);

	return (gchar **)g_ptr_array_free(array, FALSE);
}

void
gmpd_song_set_tag(GMpdSong *self, GMpdTag tag, const gchar *const *values)
{
	GHashTable *tag_table;
	gsize i;

	g_return_if_fail(GMPD_IS_SONG(self));
	g_return_if_fail(GMPD_IS_TAG(tag) && tag != GMPD_TAG_UNDEFINED);
	g_return_if_fail(values != NULL);

	tag_table = gmpd_song_get_tag_table(self, tag);

	g_hash_table_remove_all(tag_table);

	for (i = 0; values[i]; i++)
		g_hash_table_insert(tag_table, g_strdup(values[i]), NULL);

	gmpd_song_tag_changed(self, tag);
}

