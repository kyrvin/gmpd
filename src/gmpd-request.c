/* gmpd-request.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-protocol.h"
#include "gmpd-request.h"

G_DEFINE_INTERFACE(GMpdRequest, gmpd_request, G_TYPE_OBJECT)

static gboolean
gmpd_request_default_format(GMpdRequest   *self,
                            GMpdProtocol  *protocol G_GNUC_UNUSED,
                            GString       *buffer   G_GNUC_UNUSED,
                            GError       **error)
{
	GError *err = g_error_new(G_IO_ERROR,
	                          G_IO_ERROR_UNKNOWN,
	                          "%s does not implement GMpdRequest::format()",
	                          G_OBJECT_TYPE_NAME(self));

	g_critical("%s", err->message);
	g_propagate_error(error, err);

	return FALSE;
}

static void
gmpd_request_default_init(GMpdRequestIface *iface)
{
	iface->format = gmpd_request_default_format;
}

gboolean
gmpd_request_format(GMpdRequest   *self,
                    GMpdProtocol  *protocol,
                    GString       *buffer,
                    GError       **error)
{
	GMpdRequestIface *iface;

	g_return_val_if_fail(GMPD_IS_REQUEST(self), FALSE);
	g_return_val_if_fail(GMPD_IS_PROTOCOL(protocol), FALSE);
	g_return_val_if_fail(buffer != NULL, FALSE);
	g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

	iface = GMPD_REQUEST_GET_IFACE(self);

	g_return_val_if_fail(iface->format != NULL, FALSE);

	return iface->format(self, protocol, buffer, error);
}

gboolean
gmpd_request_serialize(GMpdRequest    *self,
                       GMpdProtocol   *protocol,
                       GOutputStream  *output_stream,
                       GError        **error)
{
	GString *buffer;
	gboolean result;

	g_return_val_if_fail(GMPD_IS_REQUEST(self), FALSE);
	g_return_val_if_fail(GMPD_IS_PROTOCOL(protocol), FALSE);
	g_return_val_if_fail(G_IS_OUTPUT_STREAM(output_stream), FALSE);
	g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

	buffer = g_string_new(NULL);

	if (!gmpd_request_format(self, protocol, buffer, error)) {
		g_string_free(buffer, TRUE);
		return FALSE;
	}

	result = g_output_stream_write_all(output_stream, buffer->str, buffer->len, NULL, NULL, error);

	g_string_free(buffer, TRUE);
	return result;
}
