/* gmpd-sample-format.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-sample-format.h"

static const struct {
	const gchar     *name;
	GMpdSampleFormat value;
} TABLE[] = {
	{ "*",   GMPD_SAMPLE_UNDEFINED },
	{ "8",   GMPD_SAMPLE_S8 },
	{ "16",  GMPD_SAMPLE_S16 },
	{ "24",  GMPD_SAMPLE_S24_P32 },
	{ "32",  GMPD_SAMPLE_S32 },
	{ "f",   GMPD_SAMPLE_F32 },
	{ "dsd", GMPD_SAMPLE_DSD },
};
static const gsize TABLE_LEN = G_N_ELEMENTS(TABLE);

static const GEnumValue VALUES[] = {
	{GMPD_SAMPLE_UNDEFINED, "GMPD_SAMPLE_UNDEFINED", "gmpd-sample-undefined"},
	{GMPD_SAMPLE_S8,        "GMPD_SAMPLE_S8",        "gmpd-sample-s8"},
	{GMPD_SAMPLE_S16,       "GMPD_SAMPLE_S16",       "gmpd-sample-s16"},
	{GMPD_SAMPLE_S24_P32,   "GMPD_SAMPLE_S24_P32",   "gmpd-sample-s24-p32"},
	{GMPD_SAMPLE_S32,       "GMPD_SAMPLE_S32",       "gmpd-sample-s32"},
	{GMPD_SAMPLE_F32,       "GMPD_SAMPLE_F32",       "gmpd-sample-f32"},
	{GMPD_SAMPLE_DSD,       "GMPD_SAMPLE_DSD",       "gmpd-sample-dsd"},
	{0, NULL, NULL},
};

GType
gmpd_sample_format_get_type(void)
{
	static GType type = 0;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		type = g_enum_register_static("GMpdSampleFormat", VALUES);
		g_once_init_leave(&init, 1);
	}

	return type;
}

GMpdSampleFormat
gmpd_sample_format_from_string(const gchar *s)
{
	gsize i;

	g_return_val_if_fail(s != NULL, GMPD_SAMPLE_UNDEFINED);

	for (i = 0; i < TABLE_LEN; i++) {
		if (g_strcmp0(s, TABLE[i].name) == 0)
			return TABLE[i].value;
	}

	return GMPD_SAMPLE_UNDEFINED;
}

const gchar *
gmpd_sample_format_to_string(GMpdSampleFormat format)
{
	gsize i;

	g_return_val_if_fail(GMPD_IS_SAMPLE_FORMAT(format), NULL);

	for (i = 0; i < TABLE_LEN; i++) {
		if (format == TABLE[i].value)
			return TABLE[i].name;
	}

	g_return_val_if_reached(NULL);
}

