/* gmpd-idle.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <string.h>
#include <gio/gio.h>
#include "gmpd/gmpd-idle.h"

static const gchar *const STRINGS[] = {
	"database",
	"update",
	"stored_playlist",
	"playlist",
	"player",
	"mixer",
	"output",
	"options",
	"partition",
	"sticker",
	"subscription",
	"message",
	"neighbor",
	"mount",
	NULL,
};

static const GFlagsValue VALUES[] = {
	{ GMPD_IDLE_DATABASE,     "GMPD_IDLE_DATABASE",     "gmpd_idle_database" },
	{ GMPD_IDLE_UPDATE,       "GMPD_IDLE_UPDATE",       "gmpd_idle_update" },
	{ GMPD_IDLE_PLAYLIST,     "GMPD_IDLE_PLAYLIST",     "gmpd_idle_playlist" },
	{ GMPD_IDLE_QUEUE,        "GMPD_IDLE_QUEUE",        "gmpd_idle_queue" },
	{ GMPD_IDLE_PLAYER,       "GMPD_IDLE_PLAYER",       "gmpd_idle_player" },
	{ GMPD_IDLE_MIXER,        "GMPD_IDLE_MIXER",        "gmpd_idle_mixer" },
	{ GMPD_IDLE_OUTPUT,       "GMPD_IDLE_OUTPUT",       "gmpd_idle_output" },
	{ GMPD_IDLE_OPTIONS,      "GMPD_IDLE_OPTIONS",      "gmpd_idle_options" },
	{ GMPD_IDLE_PARTITION,    "GMPD_IDLE_PARTITION",    "gmpd_idle_partition" },
	{ GMPD_IDLE_STICKER,      "GMPD_IDLE_STICKER",      "gmpd_idle_sticker" },
	{ GMPD_IDLE_SUBSCRIPTION, "GMPD_IDLE_SUBSCRIPTION", "gmpd_idle_subscription" },
	{ GMPD_IDLE_MESSAGE,      "GMPD_IDLE_MESSAGE",      "gmpd_idle_message" },
	{ GMPD_IDLE_NEIGHBOR,     "GMPD_IDLE_NEIGHBOR",     "gmpd_idle_neighbor" },
	{ GMPD_IDLE_MOUNT,        "GMPD_IDLE_MOUNT",        "gmpd_idle_mount" },
	{ 0, NULL, NULL },
};

GType
gmpd_idle_get_type(void)
{
	static GType type = 0;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		type = g_flags_register_static("GMpdIdle", VALUES);
		g_once_init_leave(&init, 1);
	}

	return type;
}

GMpdIdle
gmpd_idle_from_string(const gchar *s)
{
	GMpdIdle idle = GMPD_IDLE_NONE;
	gchar **strv;
	gsize i, j;

	g_return_val_if_fail(s != NULL, GMPD_IDLE_NONE);

	strv = g_strsplit_set(s, " \f\n\r\t\v", 0);

	for (i = 0; strv[i]; i++) {
		/* ignore empty strings */
		if (strlen(strv[i]) == 0)
			continue;

		for (j = 0; STRINGS[j]; j++) {
			if (g_strcmp0(strv[i], STRINGS[j]) == 0) {
				idle |= (1 << j);
				goto CONTINUE_OUTER;
			}
		}

		/* inner loop should have jumped to the label below if strv[i] was valid */
		g_warning("invalid idle string: %s", strv[i]);
		g_strfreev(strv);
		return GMPD_IDLE_NONE;

	CONTINUE_OUTER:
		continue;
	}

	g_strfreev(strv);
	return idle;
}

gchar *
gmpd_idle_to_string(GMpdIdle idle)
{
	GString *buffer;
	gsize i;

	idle = GMPD_IDLE_NORMALIZE(idle);

	if (idle == GMPD_IDLE_NONE)
		return NULL;

	buffer = g_string_new(NULL);

	for (i = 0; (1 << i) < GMPD_IDLE_ALL; i++) {
		if (idle & (1 << i)) {
			const gchar *format = buffer->len ? " %s" : "%s";
			g_string_append_printf(buffer, format, STRINGS[i]);
		}
	}

	return g_string_free(buffer, FALSE);
}

