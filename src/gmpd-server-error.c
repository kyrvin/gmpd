/* gmpd-server-error.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-server-error.h"

G_DEFINE_QUARK(gmpd-server-error-quark, gmpd_server_error)

static GRegex *
gmpd_server_error_regex(void)
{
	static const gchar pattern[] = "ACK \\[(\\d+)@(\\d+)] {(.+)} (.+)";
	static GRegex *regex = NULL;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		GError *err = NULL;

		regex = g_regex_new(pattern, G_REGEX_OPTIMIZE, 0, &err);
		if (!regex)
			g_error("unable to compile regex: %s", err->message);

		g_once_init_leave(&init, 1);
	}

	return regex;
}

GError *
gmpd_server_error_from_string(const gchar *srv_err_str)
{
	GRegex *regex = gmpd_server_error_regex();
	GMatchInfo *match_info;
	GError *srv_err;

	g_return_val_if_fail(srv_err_str != NULL, NULL);

	if (g_regex_match(regex, srv_err_str, 0, &match_info)) {
		gchar *code_str = g_match_info_fetch(match_info, 1);
		gchar *cmd_str = g_match_info_fetch(match_info, 3);
		gchar *msg_str = g_match_info_fetch(match_info, 4);

		gint code = g_ascii_strtoll(code_str, NULL, 10);
		if (!GMPD_IS_SERVER_ERROR(code)) {
			g_warning("unknown mpd server error code: %d", code);
			code = GMPD_SERVER_ERROR_UNKNOWN;
		}

		srv_err = g_error_new(GMPD_SERVER_ERROR, code, "%s: %s", cmd_str, msg_str);

		g_free(code_str);
		g_free(cmd_str);
		g_free(msg_str);

	} else {
		srv_err = g_error_new(GMPD_SERVER_ERROR,
		                      GMPD_SERVER_ERROR_UNKNOWN,
		                      "invalid mpd server error: %s", srv_err_str);
	}

	g_match_info_unref(match_info);
	return srv_err;
}

