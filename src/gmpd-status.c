/* gmpd-status.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-audio-format.h"
#include "gmpd/gmpd-playback-state.h"
#include "gmpd/gmpd-protocol.h"
#include "gmpd/gmpd-single-state.h"
#include "gmpd/gmpd-status.h"
#include "gmpd-request.h"
#include "gmpd-response.h"

static void gmpd_status_request_iface_init(GMpdRequestIface *iface);
static void gmpd_status_response_iface_init(GMpdResponseIface *iface);

enum {
	PROP_NONE,
	PROP_PARTITION,
	PROP_VOLUME,
	PROP_REPEAT,
	PROP_RANDOM,
	PROP_SINGLE,
	PROP_CONSUME,
	PROP_QUEUE_VERSION,
	PROP_QUEUE_LENGTH,
	PROP_PLAYBACK,
	PROP_CURRENT_SONG_POSITION,
	PROP_CURRENT_SONG_ID,
	PROP_NEXT_SONG_POSITION,
	PROP_NEXT_SONG_ID,
	PROP_ELAPSED,
	PROP_DURATION,
	PROP_BITRATE,
	PROP_CROSSFADE,
	PROP_MIXRAMP_DB,
	PROP_MIXRAMP_DELAY,
	PROP_AUDIO_FORMAT,
	PROP_DB_UPDATE_ID,
	PROP_ERROR,
	N_PROPERTIES,
};

struct _GMpdStatus {
	GObject __base__;
	gchar *partition;
	gint volume;
	gboolean repeat;
	gboolean random;
	GMpdSingleState single;
	gboolean consume;
	guint queue_version;
	guint queue_length;
	GMpdPlaybackState playback;
	guint current_song_position;
	guint current_song_id;
	guint next_song_position;
	guint next_song_id;
	gfloat elapsed;
	gfloat duration;
	guint bitrate;
	guint crossfade;
	gfloat mixramp_db;
	gfloat mixramp_delay;
	GMpdAudioFormat *audio_format;
	guint db_update_id;
	gchar *error;
};

struct _GMpdStatusClass {
	GObjectClass __base__;
};

G_DEFINE_TYPE_WITH_CODE(GMpdStatus, gmpd_status, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(GMPD_TYPE_REQUEST, gmpd_status_request_iface_init)
                        G_IMPLEMENT_INTERFACE(GMPD_TYPE_RESPONSE, gmpd_status_response_iface_init))

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};

gboolean
gmpd_status_request_format(GMpdRequest   *request  G_GNUC_UNUSED,
                           GMpdProtocol  *protocol G_GNUC_UNUSED,
                           GString       *buffer,
                           GError       **error    G_GNUC_UNUSED)
{
	g_string_append_printf(buffer, "status\n");
	return TRUE;
}

static void
gmpd_status_request_iface_init(GMpdRequestIface *iface)
{
	iface->format = gmpd_status_request_format;
}

static gboolean
gmpd_status_response_feed_pair(GMpdResponse  *response,
                               GMpdProtocol  *protocol,
                               const gchar   *key,
                               const gchar   *value,
                               GError       **error)
{
	GMpdStatus *self = GMPD_STATUS(response);

	if (g_strcmp0(key, "partition") == 0) {
		gmpd_status_set_partition(self, value);

	} else if (g_strcmp0(key, "volume") == 0) {
		gint volume = g_ascii_strtoll(value, NULL, 10);
		if (volume < 0 || volume > 100)
			volume = -1;

		gmpd_status_set_volume(self, volume);

	} else if (g_strcmp0(key, "repeat") == 0) {
		gmpd_status_set_repeat(self, value[0] =='1' && value[1] == '\0');

	} else if (g_strcmp0(key, "random") == 0) {
		gmpd_status_set_random(self, value[0] =='1' && value[1] == '\0');

	} else if (g_strcmp0(key, "single") == 0) {
		GMpdSingleState single = gmpd_single_state_from_string(value);
		gmpd_status_set_single(self, single);

	} else if (g_strcmp0(key, "consume") == 0) {
		gmpd_status_set_consume(self, value[0] =='1' && value[1] == '\0');

	} else if (g_strcmp0(key, "playlist") == 0) {
		guint queue_version = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_queue_version(self, queue_version);

	} else if (g_strcmp0(key, "playlistlength") == 0) {
		guint queue_length = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_queue_length(self, queue_length);

	} else if (g_strcmp0(key, "state") == 0) {
		GMpdPlaybackState playback = gmpd_playback_state_from_string(value);
		gmpd_status_set_playback(self, playback);

	} else if (g_strcmp0(key, "song") == 0) {
		guint position = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_current_song_position(self, position);

	} else if (g_strcmp0(key, "songid") == 0) {
		guint id = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_current_song_id(self, id);

	} else if (g_strcmp0(key, "nextsong") == 0) {
		guint position = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_next_song_position(self, position);

	} else if (g_strcmp0(key, "nextsongid") == 0) {
		guint id = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_next_song_id(self, id);

	} else if (g_strcmp0(key, "time") == 0) {
		const gchar *sep = NULL;

		if (gmpd_protocol_compare_version(protocol, 0, 16, 0) < 0) {
			gfloat elapsed = g_ascii_strtod(value, (gchar **)&sep);
			gmpd_status_set_elapsed(self, elapsed);
		}

		if (gmpd_protocol_compare_version(protocol, 0, 20, 0) < 0) {
			gfloat duration;

			if (!sep && !(sep = strchr(value, ':'))) {
				GError *err = g_error_new(G_IO_ERROR,
							  G_IO_ERROR_INVALID_DATA,
							  "%s: invalid value for key %s: %s",
							  G_OBJECT_TYPE_NAME(self), key, value);

				g_warning("%s", err->message);
				g_propagate_error(error, err);

				return FALSE;
			}

			duration = g_ascii_strtod(sep + 1, NULL);
			gmpd_status_set_duration(self, duration);
		}
	} else if (g_strcmp0(key, "elasped") == 0) {
		gfloat elapsed = g_ascii_strtod(value, NULL);
		if (elapsed < 0)
			elapsed = 0;

		gmpd_status_set_elapsed(self, elapsed);

	} else if (g_strcmp0(key, "duration") == 0) {
		gfloat duration = g_ascii_strtod(value, NULL);
		if (duration < 0)
			duration = 0;

		gmpd_status_set_duration(self, duration);

	} else if (g_strcmp0(key, "bitrate") == 0) {
		guint bitrate = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_bitrate(self, bitrate);

	} else if (g_strcmp0(key, "xfade") == 0) {
		guint crossfade = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_crossfade(self, crossfade);

	} else if (g_strcmp0(key, "mixrampdb") == 0) {
		gfloat mixramp_db = g_ascii_strtod(value, NULL);
		gmpd_status_set_mixramp_db(self, mixramp_db);

	} else if (g_strcmp0(key, "mixrampdelay") == 0) {
		gfloat mixramp_delay = g_ascii_strtod(value, NULL);
		gmpd_status_set_mixramp_delay(self, mixramp_delay);

	} else if (g_strcmp0(key, "audio") == 0) {
		GMpdAudioFormat *audio_format = gmpd_audio_format_new_from_string(value, NULL);
		gmpd_status_set_audio_format(self, audio_format);
		g_clear_object(&audio_format);

	} else if (g_strcmp0(key, "updating_db") == 0) {
		guint db_update_id = g_ascii_strtoull(value, NULL, 10);
		gmpd_status_set_db_update_id(self, db_update_id);

	} else if (g_strcmp0(key, "error") == 0) {
		gmpd_status_set_error(self, value);

	} else {
		GError *err = g_error_new(G_IO_ERROR,
		                          G_IO_ERROR_INVALID_DATA,
		                          "invalid key for %s: %s",
		                          G_OBJECT_TYPE_NAME(self), key);

		g_warning("%s", err->message);
		g_propagate_error(error, err);

		return FALSE;
	}

	return TRUE;
}

static void
gmpd_status_response_iface_init(GMpdResponseIface *iface)
{
	iface->feed_pair = gmpd_status_response_feed_pair;
}

static void
gmpd_status_set_property(GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
	GMpdStatus *self = GMPD_STATUS(object);

	switch (prop_id) {
	case PROP_PARTITION:
		gmpd_status_set_partition(self, g_value_get_string(value));
		break;

	case PROP_VOLUME:
		gmpd_status_set_volume(self, g_value_get_int(value));
		break;

	case PROP_REPEAT:
		gmpd_status_set_repeat(self, g_value_get_boolean(value));
		break;

	case PROP_RANDOM:
		gmpd_status_set_random(self, g_value_get_boolean(value));
		break;

	case PROP_SINGLE:
		gmpd_status_set_single(self, g_value_get_enum(value));
		break;

	case PROP_CONSUME:
		gmpd_status_set_consume(self, g_value_get_boolean(value));
		break;

	case PROP_QUEUE_VERSION:
		gmpd_status_set_queue_version(self, g_value_get_uint(value));
		break;

	case PROP_QUEUE_LENGTH:
		gmpd_status_set_queue_length(self, g_value_get_uint(value));
		break;

	case PROP_PLAYBACK:
		gmpd_status_set_playback(self, g_value_get_enum(value));
		break;

	case PROP_CURRENT_SONG_POSITION:
		gmpd_status_set_current_song_position(self, g_value_get_uint(value));
		break;

	case PROP_CURRENT_SONG_ID:
		gmpd_status_set_current_song_id(self, g_value_get_uint(value));
		break;

	case PROP_NEXT_SONG_POSITION:
		gmpd_status_set_next_song_position(self, g_value_get_uint(value));
		break;

	case PROP_NEXT_SONG_ID:
		gmpd_status_set_next_song_id(self, g_value_get_uint(value));
		break;

	case PROP_ELAPSED:
		gmpd_status_set_elapsed(self, g_value_get_float(value));
		break;

	case PROP_DURATION:
		gmpd_status_set_duration(self, g_value_get_float(value));
		break;

	case PROP_BITRATE:
		gmpd_status_set_bitrate(self, g_value_get_uint(value));
		break;

	case PROP_CROSSFADE:
		gmpd_status_set_crossfade(self, g_value_get_uint(value));
		break;

	case PROP_MIXRAMP_DB:
		gmpd_status_set_mixramp_db(self, g_value_get_float(value));
		break;

	case PROP_MIXRAMP_DELAY:
		gmpd_status_set_mixramp_delay(self, g_value_get_float(value));
		break;

	case PROP_AUDIO_FORMAT:
		gmpd_status_set_audio_format(self, g_value_get_object(value));
		break;

	case PROP_DB_UPDATE_ID:
		gmpd_status_set_db_update_id(self, g_value_get_uint(value));
		break;

	case PROP_ERROR:
		gmpd_status_set_error(self, g_value_get_string(value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_status_get_property(GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
	GMpdStatus *self = GMPD_STATUS(object);

	switch (prop_id) {
	case PROP_PARTITION:
		g_value_take_string(value, gmpd_status_get_partition(self));
		break;

	case PROP_VOLUME:
		g_value_set_int(value, gmpd_status_get_volume(self));
		break;

	case PROP_REPEAT:
		g_value_set_boolean(value, gmpd_status_get_repeat(self));
		break;

	case PROP_RANDOM:
		g_value_set_boolean(value, gmpd_status_get_random(self));
		break;

	case PROP_SINGLE:
		g_value_set_enum(value, gmpd_status_get_single(self));
		break;

	case PROP_CONSUME:
		g_value_set_boolean(value, gmpd_status_get_consume(self));
		break;

	case PROP_QUEUE_VERSION:
		g_value_set_uint(value, gmpd_status_get_queue_version(self));
		break;

	case PROP_QUEUE_LENGTH:
		g_value_set_uint(value, gmpd_status_get_queue_length(self));
		break;

	case PROP_PLAYBACK:
		g_value_set_enum(value, gmpd_status_get_playback(self));
		break;

	case PROP_CURRENT_SONG_POSITION:
		g_value_set_uint(value, gmpd_status_get_current_song_position(self));
		break;

	case PROP_CURRENT_SONG_ID:
		g_value_set_uint(value, gmpd_status_get_current_song_id(self));
		break;

	case PROP_NEXT_SONG_POSITION:
		g_value_set_uint(value, gmpd_status_get_next_song_position(self));
		break;

	case PROP_NEXT_SONG_ID:
		g_value_set_uint(value, gmpd_status_get_next_song_id(self));
		break;

	case PROP_ELAPSED:
		g_value_set_float(value, gmpd_status_get_elapsed(self));
		break;

	case PROP_DURATION:
		g_value_set_float(value, gmpd_status_get_duration(self));
		break;

	case PROP_BITRATE:
		g_value_set_uint(value, gmpd_status_get_bitrate(self));
		break;

	case PROP_CROSSFADE:
		g_value_set_uint(value, gmpd_status_get_crossfade(self));
		break;

	case PROP_MIXRAMP_DB:
		g_value_set_float(value, gmpd_status_get_mixramp_db(self));
		break;

	case PROP_MIXRAMP_DELAY:
		g_value_set_float(value, gmpd_status_get_mixramp_delay(self));
		break;

	case PROP_AUDIO_FORMAT:
		g_value_take_object(value, gmpd_status_get_audio_format(self));
		break;

	case PROP_DB_UPDATE_ID:
		g_value_set_uint(value, gmpd_status_get_db_update_id(self));
		break;

	case PROP_ERROR:
		g_value_take_string(value, gmpd_status_get_error(self));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_status_finalize(GObject *object)
{
	GMpdStatus *self = GMPD_STATUS(object);

	g_clear_pointer(&self->partition, g_free);
	g_clear_object(&self->audio_format);
	g_clear_pointer(&self->error, g_free);

	G_OBJECT_CLASS(gmpd_status_parent_class)->finalize(object);
}

static void
gmpd_status_class_init(GMpdStatusClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->set_property = gmpd_status_set_property;
	object_class->get_property = gmpd_status_get_property;
	object_class->finalize = gmpd_status_finalize;

	PROPERTIES[PROP_PARTITION] =
		g_param_spec_string("partition",
		                    "Partition",
		                    "Name of the current partition.",
		                    NULL,
		                    G_PARAM_READWRITE |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);
	PROPERTIES[PROP_VOLUME] =
		g_param_spec_int("volume",
		                 "Volume",
		                 "Current playback volume.",
		                 -1, 100, -1,
		                 G_PARAM_READWRITE |
		                 G_PARAM_EXPLICIT_NOTIFY |
		                 G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_REPEAT] =
		g_param_spec_boolean("repeat",
		                     "Repeat",
		                     "Current state of the repeat option.",
		                     FALSE,
		                     G_PARAM_READWRITE |
		                     G_PARAM_EXPLICIT_NOTIFY |
		                     G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_RANDOM] =
		g_param_spec_boolean("random",
		                     "Random",
		                     "Current state of the random option.",
		                     FALSE,
		                     G_PARAM_READWRITE |
		                     G_PARAM_EXPLICIT_NOTIFY |
		                     G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_SINGLE] =
		g_param_spec_enum("single",
		                  "Single",
		                  "Current state of the single option.",
		                  GMPD_TYPE_SINGLE_STATE,
		                  GMPD_SINGLE_UNDEFINED,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_CONSUME] =
		g_param_spec_boolean("consume",
		                     "Consume",
		                     "Current state of the consume option.",
		                     FALSE,
		                     G_PARAM_READWRITE |
		                     G_PARAM_EXPLICIT_NOTIFY |
		                     G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_QUEUE_VERSION] =
		g_param_spec_uint("queue-version",
		                  "Queue Version",
		                  "The queue version number.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_QUEUE_LENGTH] =
		g_param_spec_uint("queue-length",
		                  "Queue Length",
		                  "The length of the queue.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_PLAYBACK] =
		g_param_spec_enum("playback",
		                  "Playback",
		                  "Current playback state of the server.",
		                  GMPD_TYPE_PLAYBACK_STATE,
		                  GMPD_PLAYBACK_UNDEFINED,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_CURRENT_SONG_POSITION] =
		g_param_spec_uint("current-song-position",
		                  "Current Song Position",
		                  "Position of the currently playing song.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_CURRENT_SONG_ID] =
		g_param_spec_uint("current-song-id",
		                  "Current Song ID",
		                  "Queue ID of the currently playing song.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_NEXT_SONG_POSITION] =
		g_param_spec_uint("next-song-position",
		                  "Next Song Position",
		                  "Position of the next song in the queue.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_NEXT_SONG_ID] =
		g_param_spec_uint("next-song-id",
		                  "Next Song Id",
		                  "ID of the next song in the queue.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_ELAPSED] =
		g_param_spec_float("elapsed",
		                   "Elapsed",
		                   "Time elapsed within the currently playing song.",
		                   0, G_MAXFLOAT, 0,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_DURATION] =
		g_param_spec_float("duration",
		                   "Duration",
		                   "Duration of the currently playing song.",
		                   0, G_MAXFLOAT, 0,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_BITRATE] =
		g_param_spec_uint("bitrate",
		                  "Bitrate",
		                  "Current bitrate in kbps.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_CROSSFADE] =
		g_param_spec_uint("crossfade",
		                  "Crossfade",
		                  "Crossfade duration in seconds.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_MIXRAMP_DB] =
		g_param_spec_float("mixramp-db",
		                   "MixRamp Db",
		                   "MixRamp threshold in dB.",
		                   -G_MAXFLOAT, G_MAXFLOAT, 0,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_MIXRAMP_DELAY] =
		g_param_spec_float("mixramp-delay",
		                   "MixRamp Delay",
		                   "MixRamp delay in seconds.",
		                   -G_MAXFLOAT, G_MAXFLOAT, 0,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_AUDIO_FORMAT] =
		g_param_spec_object("audio-format",
		                    "Audio Format",
		                    "Audio format emitted by the decoder plugin.",
		                    GMPD_TYPE_AUDIO_FORMAT,
		                    G_PARAM_READWRITE |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_DB_UPDATE_ID] =
		g_param_spec_uint("db-update-id",
		                  "Db Update Id",
		                  "Database update job id.",
		                  0, G_MAXUINT, 0,
		                  G_PARAM_READWRITE |
		                  G_PARAM_EXPLICIT_NOTIFY |
		                  G_PARAM_STATIC_STRINGS);

	PROPERTIES[PROP_ERROR] =
		g_param_spec_string("error",
		                    "Error",
		                    "Error message returned from the server.",
		                    NULL,
		                    G_PARAM_READWRITE |
		                    G_PARAM_EXPLICIT_NOTIFY |
		                    G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);
}

static void
gmpd_status_init(GMpdStatus *self)
{
	self->partition = NULL;
	self->volume = -1;
	self->repeat = FALSE;
	self->random = FALSE;
	self->single = GMPD_SINGLE_UNDEFINED;
	self->consume = FALSE;
	self->queue_version = 0;
	self->queue_length = 0;
	self->playback = GMPD_PLAYBACK_UNDEFINED;
	self->current_song_position = 0;
	self->current_song_id = 0;
	self->next_song_position = 0;
	self->next_song_id = 0;
	self->elapsed = 0;
	self->duration = 0;
	self->bitrate = 0;
	self->crossfade = 0;
	self->mixramp_db = 0;
	self->mixramp_delay = 0;
	self->audio_format = NULL;
	self->db_update_id = 0;
	self->error = NULL;
}

GMpdStatus *
gmpd_status_new(void)
{
	return g_object_new(GMPD_TYPE_STATUS, NULL);
}

void
gmpd_status_set_partition(GMpdStatus *self, const gchar *partition)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	g_free(self->partition);
	self->partition = g_strdup(partition);

	g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_PARTITION]);
}

void
gmpd_status_set_volume(GMpdStatus *self, gint volume)
{
	g_return_if_fail(GMPD_IS_STATUS(self));
	g_return_if_fail(volume >= -1 && volume <= 100);

	if (self->volume != volume) {
		self->volume = volume;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_VOLUME]);
	}
}

void
gmpd_status_set_repeat(GMpdStatus *self, gboolean repeat)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->repeat != repeat) {
		self->repeat = repeat;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_REPEAT]);
	}
}

void
gmpd_status_set_random(GMpdStatus *self, gboolean random)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->random != random) {
		self->random = random;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_RANDOM]);
	}
}

void
gmpd_status_set_single(GMpdStatus *self, GMpdSingleState single)
{
	g_return_if_fail(GMPD_IS_STATUS(self));
	g_return_if_fail(GMPD_IS_SINGLE_STATE(single));

	if (self->single != single) {
		self->single = single;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_SINGLE]);
	}
}

void
gmpd_status_set_consume(GMpdStatus *self, gboolean consume)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->consume != consume) {
		self->consume = consume;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_CONSUME]);
	}
}

void
gmpd_status_set_queue_version(GMpdStatus *self, guint queue_version)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->queue_version != queue_version) {
		self->queue_version = queue_version;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_QUEUE_VERSION]);
	}
}

void
gmpd_status_set_queue_length(GMpdStatus *self, guint queue_length)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->queue_length != queue_length) {
		self->queue_length = queue_length;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_QUEUE_LENGTH]);
	}
}

void
gmpd_status_set_playback(GMpdStatus *self, GMpdPlaybackState playback)
{
	g_return_if_fail(GMPD_IS_STATUS(self));
	g_return_if_fail(GMPD_IS_PLAYBACK_STATE(playback));

	if (self->playback != playback) {
		self->playback = playback;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_PLAYBACK]);
	}
}

void
gmpd_status_set_current_song_position(GMpdStatus *self, guint current_song_position)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->current_song_position != current_song_position) {
		self->current_song_position = current_song_position;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_CURRENT_SONG_POSITION]);
	}
}

void
gmpd_status_set_current_song_id(GMpdStatus *self, guint current_song_id)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->current_song_id != current_song_id) {
		self->current_song_id = current_song_id;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_CURRENT_SONG_ID]);
	}
}

void
gmpd_status_set_next_song_position(GMpdStatus *self, guint next_song_position)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->next_song_position != next_song_position) {
		self->next_song_position = next_song_position;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_NEXT_SONG_POSITION]);
	}
}

void
gmpd_status_set_next_song_id(GMpdStatus *self, guint next_song_id)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->next_song_id != next_song_id) {
		self->next_song_id = next_song_id;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_NEXT_SONG_ID]);
	}
}

void
gmpd_status_set_elapsed(GMpdStatus *self, gfloat elapsed)
{
	g_return_if_fail(GMPD_IS_STATUS(self));
	g_return_if_fail(elapsed >= 0);

	if (self->elapsed != elapsed) {
		self->elapsed = elapsed;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_ELAPSED]);
	}
}

void
gmpd_status_set_duration(GMpdStatus *self, gfloat duration)
{
	g_return_if_fail(GMPD_IS_STATUS(self));
	g_return_if_fail(duration >= 0);

	if (self->duration != duration) {
		self->duration = duration;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_DURATION]);
	}
}

void
gmpd_status_set_bitrate(GMpdStatus *self, guint bitrate)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->bitrate != bitrate) {
		self->bitrate = bitrate;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_BITRATE]);
	}
}

void
gmpd_status_set_crossfade(GMpdStatus *self, guint crossfade)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->crossfade != crossfade) {
		self->crossfade = crossfade;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_CROSSFADE]);
	}
}

void
gmpd_status_set_mixramp_db(GMpdStatus *self, gfloat mixramp_db)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->mixramp_db != mixramp_db) {
		self->mixramp_db = mixramp_db;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_MIXRAMP_DB]);
	}
}

void
gmpd_status_set_mixramp_delay(GMpdStatus *self, gfloat mixramp_delay)
{
	g_return_if_fail(GMPD_IS_STATUS(self));
	g_return_if_fail(mixramp_delay >= 0);

	if (self->mixramp_delay != mixramp_delay) {
		self->mixramp_delay = mixramp_delay;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_MIXRAMP_DELAY]);
	}
}

void
gmpd_status_set_audio_format(GMpdStatus *self, GMpdAudioFormat *audio_format)
{
	g_return_if_fail(GMPD_IS_STATUS(self));
	g_return_if_fail(GMPD_IS_AUDIO_FORMAT(audio_format));

	g_clear_object(&self->audio_format);
	self->audio_format = audio_format ? g_object_ref(audio_format) : NULL;

	g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_AUDIO_FORMAT]);
}

void
gmpd_status_set_db_update_id(GMpdStatus *self, guint db_update_id)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	if (self->db_update_id != db_update_id) {
		self->db_update_id = db_update_id;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_DB_UPDATE_ID]);
	}
}

void
gmpd_status_set_error(GMpdStatus *self, const gchar *error)
{
	g_return_if_fail(GMPD_IS_STATUS(self));

	g_free(self->error);
	self->error = g_strdup(error);

	g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_ERROR]);
}

gchar *
gmpd_status_get_partition(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), NULL);
	return g_strdup(self->partition);
}

gint
gmpd_status_get_volume(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), -1);
	return self->volume;
}

gboolean
gmpd_status_get_repeat(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), FALSE);
	return self->repeat;
}

gboolean
gmpd_status_get_random(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), FALSE);
	return self->random;
}

GMpdSingleState
gmpd_status_get_single(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), GMPD_SINGLE_UNDEFINED);
	return self->single;
}

gboolean
gmpd_status_get_consume(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), FALSE);
	return self->consume;
}

guint
gmpd_status_get_queue_version(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->queue_version;
}

guint
gmpd_status_get_queue_length(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->queue_length;
}

GMpdPlaybackState
gmpd_status_get_playback(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), GMPD_PLAYBACK_UNDEFINED);
	return self->playback;
}

guint
gmpd_status_get_current_song_position(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->current_song_position;
}

guint
gmpd_status_get_current_song_id(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->current_song_id;
}

guint
gmpd_status_get_next_song_position(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->next_song_position;
}

guint
gmpd_status_get_next_song_id(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->next_song_id;
}

gfloat
gmpd_status_get_elapsed(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->elapsed;
}

gfloat
gmpd_status_get_duration(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->duration;
}

guint
gmpd_status_get_bitrate(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->bitrate;
}

guint
gmpd_status_get_crossfade(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->crossfade;
}

gfloat
gmpd_status_get_mixramp_db(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->mixramp_db;
}

gfloat
gmpd_status_get_mixramp_delay(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->mixramp_delay;
}

GMpdAudioFormat *
gmpd_status_get_audio_format(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), NULL);
	return self->audio_format ? g_object_ref(self->audio_format) : NULL;
}

guint
gmpd_status_get_db_update_id(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), 0);
	return self->db_update_id;
}

gchar *
gmpd_status_get_error(GMpdStatus *self)
{
	g_return_val_if_fail(GMPD_IS_STATUS(self), NULL);
	return g_strdup(self->error);
}

