/* gmpd-request.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_REQUEST_H
#define GMPD_REQUEST_H

#include <gio/gio.h>
#include <gmpd/gmpd-protocol.h>

G_BEGIN_DECLS

#define GMPD_TYPE_REQUEST \
	(gmpd_request_get_type())

#define GMPD_REQUEST(inst) \
	(G_TYPE_CHECK_INSTANCE_CAST((inst), GMPD_TYPE_REQUEST, GMpdRequest))

#define GMPD_IS_REQUEST(inst) \
	(G_TYPE_CHECK_INSTANCE_TYPE((inst), GMPD_TYPE_REQUEST))

#define GMPD_REQUEST_GET_IFACE(inst) \
	(G_TYPE_INSTANCE_GET_INTERFACE((inst), GMPD_TYPE_REQUEST, GMpdRequestIface))

typedef struct _GMpdRequest      GMpdRequest;
typedef struct _GMpdRequestIface GMpdRequestIface;
typedef GMpdRequestIface GMpdRequestInterface;

struct _GMpdRequestIface {
	GTypeInterface __base__;

	gboolean (*format) (GMpdRequest   *self,
	                    GMpdProtocol  *protocol,
	                    GString       *buffer,
	                    GError       **error);
};

GType gmpd_request_get_type(void);

gboolean gmpd_request_format(GMpdRequest   *request,
                             GMpdProtocol  *protocol,
                             GString       *buffer,
                             GError       **error);

gboolean gmpd_request_serialize(GMpdRequest    *request,
                                GMpdProtocol   *protocol,
                                GOutputStream  *output_stream,
                                GError        **error);

G_END_DECLS

#endif /* GMPD_REQUEST_H */

