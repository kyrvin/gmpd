/* gmpd-unit-response.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd-response.h"
#include "gmpd-unit-response.h"

static void gmpd_unit_response_response_iface_init(GMpdResponseIface *iface);

struct _GMpdUnitResponse {
	GObject __base__;
};

struct _GMpdUnitResponseClass {
	GObjectClass __base__;
};

G_DEFINE_TYPE_WITH_CODE(GMpdUnitResponse, gmpd_unit_response, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(GMPD_TYPE_UNIT_RESPONSE, gmpd_unit_response_response_iface_init))

static void
gmpd_unit_response_response_iface_init(GMpdResponseIface *iface G_GNUC_UNUSED)
{
}

static GObject *
gmpd_unit_response_constructor(GType                  type,
                               guint                  n_params,
                               GObjectConstructParam *params)
{
	static GMpdUnitResponse *singleton = NULL;
	static gsize init = 0;

	if (g_once_init_enter(&init)) {
		GObjectClass *parent_class = G_OBJECT_CLASS(gmpd_unit_response_parent_class);
		singleton = GMPD_UNIT_RESPONSE(parent_class->constructor(type, n_params, params));
		g_once_init_leave(&init, 1);
	}

	return G_OBJECT(g_object_ref(singleton));
}

static void
gmpd_unit_response_class_init(GMpdUnitResponseClass *klass)
{
	G_OBJECT_CLASS(klass)->constructor = gmpd_unit_response_constructor;
}

static void
gmpd_unit_response_init(GMpdUnitResponse *self G_GNUC_UNUSED)
{
}

GMpdUnitResponse *
gmpd_unit_response_new(void)
{
	return g_object_new(GMPD_TYPE_UNIT_RESPONSE, NULL);
}

