/* gmpd-cond.h - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GMPD_COND_H
#define GMPD_COND_H

#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct _GMpdCond {
	GMutex mutex;
	GCond  cond;
	gboolean is_ready;
} GMpdCond;

void gmpd_cond_init(GMpdCond *self);
void gmpd_cond_clear(GMpdCond *self);
void gmpd_cond_wait(GMpdCond *self);
void gmpd_cond_signal(GMpdCond *self);

G_END_DECLS

#endif /* GMPD_COND_H */

