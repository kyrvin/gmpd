/* gmpd-task-data.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd-cond.h"
#include "gmpd-request.h"
#include "gmpd-response.h"
#include "gmpd-task-data.h"

GMpdTaskData *
gmpd_task_data_new(GMpdRequest  *request,
                   GMpdResponse *response,
                   GMpdCond     *cond)
{
	GMpdTaskData *data = g_slice_new(GMpdTaskData);

	data->request = request;
	data->response = response;
	data->cond = cond;

	return data;
}

void
gmpd_task_data_free(GMpdTaskData *data)
{
	g_clear_object(&data->request);
	g_clear_object(&data->response);

	/* data->cond is borrowed, not owned */

	g_slice_free(GMpdTaskData, data);
}

void
gmpd_task_return(GTask *task)
{
	GMpdTaskData *data = g_task_get_task_data(task);

	if (data->response)
		g_task_return_pointer(task, g_object_ref(data->response), g_object_unref);
	else
		g_task_return_boolean(task, TRUE);

	if (data->cond)
		gmpd_cond_signal(data->cond);
}

void
gmpd_task_return_error(GTask *task, GError *err)
{
	GMpdTaskData *data = g_task_get_task_data(task);

	g_task_return_error(task, err);

	if (data->cond)
		gmpd_cond_signal(data->cond);
}

