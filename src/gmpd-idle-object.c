/* gmpd-idle-object.c - Copyright 2021 Patrick Keating <kyrvin3 at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <gio/gio.h>
#include "gmpd/gmpd-idle.h"
#include "gmpd/gmpd-protocol.h"
#include "gmpd-idle-object.h"
#include "gmpd-request.h"
#include "gmpd-response.h"

static void gmpd_idle_object_request_iface_init(GMpdRequestIface *iface);
static void gmpd_idle_object_response_iface_init(GMpdResponseIface *iface);

enum {
	PROP_NONE,
	PROP_EVENTS,
	N_PROPERTIES,
};

struct _GMpdIdleObject {
	GObject __base__;
	GMpdIdle  events;
};

struct _GMpdIdleObjectClass {
	GObjectClass __base__;
};

G_DEFINE_TYPE_WITH_CODE(GMpdIdleObject, gmpd_idle_object, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(GMPD_TYPE_REQUEST, gmpd_idle_object_request_iface_init)
                        G_IMPLEMENT_INTERFACE(GMPD_TYPE_RESPONSE, gmpd_idle_object_response_iface_init))

static GParamSpec *PROPERTIES[N_PROPERTIES] = {NULL};

static gboolean
gmpd_idle_object_request_format(GMpdRequest   *request,
                                GMpdProtocol  *protocol G_GNUC_UNUSED,
                                GString       *buffer,
                                GError       **error    G_GNUC_UNUSED)
{
	GMpdIdleObject *self = GMPD_IDLE_OBJECT(request);

	/* TODO: drop events unsupported by the protocol version */

	if (self->events == GMPD_IDLE_NONE || self->events == GMPD_IDLE_ALL) {
		g_string_append_printf(buffer, "idle\n");

	} else {
		gchar *events_str = gmpd_idle_to_string(self->events);
		g_string_append_printf(buffer, "idle %s\n", events_str);
		g_free(events_str);
	}

	return TRUE;
}

static void
gmpd_idle_object_request_iface_init(GMpdRequestIface *iface)
{
	iface->format = gmpd_idle_object_request_format;
}

static gboolean
gmpd_idle_object_response_feed_pair(GMpdResponse  *response,
                                    GMpdProtocol  *protocol G_GNUC_UNUSED,
                                    const gchar   *key,
                                    const gchar   *value,
                                    GError       **error)
{
	GMpdIdleObject *self = GMPD_IDLE_OBJECT(response);
	GMpdIdle event;

	if (g_strcmp0(key, "changed") != 0) {
		GError *err = g_error_new(G_IO_ERROR,
		                          G_IO_ERROR_INVALID_DATA,
		                          "invalid key for %s: %s",
		                          G_OBJECT_TYPE_NAME(self), key);

		g_warning("%s", err->message);
		g_propagate_error(error, err);

		return FALSE;
	}

	event = gmpd_idle_from_string(value);
	if (event == GMPD_IDLE_NONE) {
		GError *err = g_error_new(G_IO_ERROR,
		                          G_IO_ERROR_INVALID_DATA,
		                          "%s: invalid value for key %s: %s",
		                          G_OBJECT_TYPE_NAME(self), key, value);

		g_warning("%s", err->message);
		g_propagate_error(error, err);

		return FALSE;
	}

	self->events = GMPD_IDLE_NORMALIZE(self->events | event);
	g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_EVENTS]);

	return TRUE;
}

static void
gmpd_idle_object_response_iface_init(GMpdResponseIface *iface)
{
	iface->feed_pair = gmpd_idle_object_response_feed_pair;
}

static void
gmpd_idle_object_set_property(GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
	GMpdIdleObject *self = GMPD_IDLE_OBJECT(object);

	switch (prop_id) {
	case PROP_EVENTS:
		gmpd_idle_object_set_events(self, g_value_get_flags(value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_idle_object_get_property(GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
	GMpdIdleObject *self = GMPD_IDLE_OBJECT(object);

	switch (prop_id) {
	case PROP_EVENTS:
		g_value_set_flags(value, gmpd_idle_object_get_events(self));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void
gmpd_idle_object_class_init(GMpdIdleObjectClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->set_property = gmpd_idle_object_set_property;
	object_class->get_property = gmpd_idle_object_get_property;

	PROPERTIES[PROP_EVENTS] =
		g_param_spec_flags("events",
		                   "Events",
		                   "The idle events held by the object",
		                   GMPD_TYPE_IDLE,
		                   GMPD_IDLE_NONE,
		                   G_PARAM_READWRITE |
		                   G_PARAM_EXPLICIT_NOTIFY |
		                   G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties(object_class, N_PROPERTIES, PROPERTIES);
}

static void
gmpd_idle_object_init(GMpdIdleObject *self)
{
	self->events = GMPD_IDLE_NONE;
}

GMpdIdleObject *
gmpd_idle_object_new(GMpdIdle events)
{
	return g_object_new(GMPD_TYPE_IDLE_OBJECT, "events", events, NULL);
}

void
gmpd_idle_object_set_events(GMpdIdleObject *self, GMpdIdle events)
{
	g_return_if_fail(GMPD_IS_IDLE_OBJECT(self));

	events = GMPD_IDLE_NORMALIZE(events);
	if (self->events != events) {
		self->events = events;
		g_object_notify_by_pspec(G_OBJECT(self), PROPERTIES[PROP_EVENTS]);
	}
}

GMpdIdle
gmpd_idle_object_get_events(GMpdIdleObject *self)
{
	g_return_val_if_fail(GMPD_IS_IDLE_OBJECT(self), GMPD_IDLE_NONE);
	return GMPD_IDLE_NORMALIZE(self->events);
}

